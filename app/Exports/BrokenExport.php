<?php

namespace App\Exports;

use App\Models\Broken;
use App\Repositories\BrokenRepository;
use Maatwebsite\Excel\Concerns\FromCollection;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BrokenExport implements FromCollection, ShouldAutoSize
{
  public function collection()
  {
  	$brokens = Broken::whereNull('deleted_at')->get();
  	$broken[] = array();
    foreach ($brokens as $row) {
    	$broken[] = array(
    		'0' => '#',
    		'1' => 'Tên phiếu',
    		'2' => 'Mã phiếu',
    		'3' => 'Ngày tạo',
    	);
      $broken[] = array(
        '0' => $row->id,
        '1' => $row->name,
        '2' => $row->code,
        '3' => $row->create_date,
      );
      $broken[] = array('0'=>'Thiết bị', '1'=>'Số lượng');
      foreach ($row->device()->get() as $device) {
      	$broken[] = array(
      		'0' => $device->name,
      		'1' => $device->pivot->amount,
      	);
      }
      $broken[] = array('0'=>'');
    }
    return (collect($broken));
  }
}
