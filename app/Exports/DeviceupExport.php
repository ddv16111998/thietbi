<?php

namespace App\Exports;

use App\Models\Deviceup;
use App\Repositories\DeviceupRepository;
use Maatwebsite\Excel\Concerns\FromCollection;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DeviceupExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $deviceups = Deviceup::whereNull('deleted_at')->get();
	  	$deviceup[] = array();
	    foreach ($deviceups as $row) {
	    	$deviceup[] = array(
	    		'0' => '#',
	    		'1' => 'Tên phiếu',
	    		'2' => 'Ngày tạo',
	    	);
	      $deviceup[] = array(
	        '0' => $row->id,
	        '1' => $row->name,
	        '2' => $row->create_date,
	      );
	      $deviceup[] = array('0'=>'Thiết bị', '1'=>'Số lượng');
	      foreach ($row->device()->get() as $device) {
	      	$deviceup[] = array(
	      		'0' => $device->name,
	      		'1' => $device->pivot->amount,
	      		'2' => $device->pivot->note,
	      	);
	      }
	      $deviceup[] = array('0'=>'');
	    }
	    return (collect($deviceup));
    }
}
