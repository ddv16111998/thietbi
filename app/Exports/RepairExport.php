<?php

namespace App\Exports;

use App\Models\Device_Repair;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RepairExport implements FromCollection, ShouldAutoSize
{
    public function collection()
    {
        $repairs = Device_Repair::whereNull('deleted_at')->get();
        $stt = 1;
        $repair[] = array();
        foreach ($repairs as $row) {
            $repair[] = array(
                '0' => 'Stt',
                '1' => 'Tên phiếu',
                '2' => 'Ngày lập',
            );
            $repair[] = array(
                '0' => $stt++,
                '1' => $row->ballot,
                '2' => $row->start_day,
            );
            $repair[] = array('0'=> '','1'=>'Thiết bị', '2'=>'Số lượng','3'=>'Đơn giá sửa');
            foreach ($row->device()->get() as $device) {
                $repair[] = array(
                    '0' => '',
                    '1' => $device->name,
                    '2' => $device->pivot->amount,
                    '3' => $device->pivot->price,
                );
            }
            $repair[] = array('0'=>'');
        }
        return (collect($repair));
    }
}
