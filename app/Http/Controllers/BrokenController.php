<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\CategoryRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\BrokenRepository;
use App\Repositories\BrokenDeviceRepository;
use App\Http\Requests\StoreCreateBrokenPost;
use Carbon\Carbon;

use App\Exports\BrokenExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class BrokenController extends Controller
{
    protected $repositories;

    function __construct(BrokenRepository $brokenRepository)
    {
            $this->repositories = $brokenRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
        $data = $this->repositories->getDataIndex();
        return DataTables::of($data)
        ->editColumn('status', function ($data)
        {
          return ($data->status == 0) ? "Hỏng" : "Mất";
        })
        ->addColumn('device', function ($data) {
            $dv = $data->device()->get();
            $ss = null;
            foreach ($dv as $v) {
                $ss .= $v->name . '</br>';
            }
            return $ss;
        })
        ->addColumn('action', function ($data) {
          $string = '
          <a href="'.route('admin.broken.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
          <a href="javascript:void(0)" data-href="'.route('admin.broken.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
          ';
          return $string;
        })
        ->rawColumns(['action', 'device'])
        ->toJson();
        }
      return view('broken.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, CategoryRepository $cate, DeviceRepository $device)
    {
        if($request->ajax()){
          $data = ' <div class="row form-group">
                        <div class="col-6">
                            <select class="device-ajax form-control" name="device_id[]">';
            foreach ($device->getDataIndex() as $val) {
                $data .=        '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
            $data .= '      </select>
                        </div>
                        <div class="col-3">
                            <input type="number" name="amount[]" class="form-control" placeholder="Số lượng" min="0">
                        </div>
                        <div class="col-3">
                            <select class="device-ajax form-control" name="status_device[]">
                                <option value="0">Sửa chữa</option>
                                <option value="1">Thanh lý</option>
                            </select>
                        </div>
                    </div>';
            return $data;
        }
        return view('broken.create', ['cates'=>$cate->getDataIndex(), 'devices'=>$device->getDataIndex()]);
    }

    public function saveData($request, $id = null)
    {
        $create_date=  Carbon::createFromFormat('d/m/Y', $request->create_date);
        $data = [
            'name'      => $request->name,
            'code'      => $request->code,
            'status'    => $request->status,
            'create_date' => $create_date->toDateString(),
            'note'      => $request->note,
        ];

        $rel_data = [
            'device_id' => $request->device_id,
            'amount'    => $request->amount,
            'status'    => $request->status_device,
        ];
        return $this->repositories->updateOrCreate($data, $rel_data, $id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCreateBrokenPost $request)
    {
      if ($this->saveData($request)) {
        return redirect()->route('admin.broken.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
      }else{
        return redirect()->route('admin.broken.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CategoryRepository $cate, DeviceRepository $device)
    {
        $broken = $this->repositories->getById($id);
        $devices = $broken->device()->get();
        return view('broken.edit', ['devices_sl' => $device->getDataIndex(), 'cates'=>$cate->getDataIndex(), 'broken'=>$broken, 'devices'=>$devices]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreCreateBrokenPost $request)
    {
        if ($this->saveData($request, $id)) {
            return redirect()->route('admin.broken.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        }else{
            return redirect()->route('admin.broken.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $this->repositories->destroy($id);
      return redirect()->route('admin.broken.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }

    public function exportExcel()
    {
        return Excel::download(new BrokenExport, 'Danh sách hỏng mất thiết bị.xlsx');
    }
}
