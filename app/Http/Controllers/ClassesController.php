<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\StoreCreateClassPost;
use Yajra\Datatables\Datatables;
use App\Repositories\ClassesRepository;
use App\Repositories\BlockRepository;
use App\Repositories\SchoolRepository;

class ClassesController extends Controller
{
    protected $repositories;
    protected $schoolRepositories;
    protected $blockRepositories;

    function __construct(ClassesRepository $classesRepository)
    {
            $this->repositories = $classesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
            ->addColumn('school', function ($data) {
                return $data->school->name;
            })
            ->addColumn('block', function ($data) {
                return $data->block->name;
            })
            ->addColumn('status', function ($data) {
                if($data->status == 1)
                    return '<code class="text-success">Hoạt động</code>';
                if($data->status == 0)
                    return '<code class="text-danger">Ẩn</code>';
            })
            ->addColumn('action', function ($data) {
                $string = '
                <a href="'.route('admin.classes.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                <a href="javascript:void(0)" data-href="'.route('admin.classes.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                ';
                return $string;
            })
            ->rawColumns(['action', 'status', 'school', 'block'])
            ->toJson();
        }
        return view('classes.index');
    }

    public function saveData($request, $id = null)
    {
        $data = [
            'name' => $request->name,
            'school_id' => $request->school_id,
            'block_id' => $request->block_id,
            'school_year' => $request->start .'-'.$request->end,
            'gvcn_name' => $request->gvcn_name,
            'gvcn_phone' => $request->gvcn_phone,
            'gvcn_email' => $request->gvcn_email,
            'description' => $request->description,
        ];
        if (isset($request->status)) {
            $data += ['status' => 1,];
        }else{
            $data += ['status' => 0,];
        }
        return $this->repositories->updateOrCreate($data, $id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SchoolRepository $schoolRepository, BlockRepository $blockRepository)
    {
        $schools = $schoolRepository->getDataIndex();
        $blocks = $blockRepository->getDataIndex();
        return view('classes.create', ['schools'=>$schools, 'blocks'=>$blocks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCreateClassPost $request)
    {
        if ($this->saveData($request)) {
            return redirect()->route('admin.classes.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
        }else{
            return redirect()->route('admin.classes.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SchoolRepository $schoolRepository, BlockRepository $blockRepository)
    {
        $classes = $this->repositories->getDataEdit($id);
        $schools = $schoolRepository->getDataIndex();
        $blocks = $blockRepository->getDataIndex();
        return view('classes.edit', ['classes'=>$classes, 'schools'=>$schools, 'blocks'=>$blocks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($this->saveData($request, $request->id)) {
            return redirect()->route('admin.classes.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        }else{
            return redirect()->route('admin.classes.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repositories->destroy($id);
        return redirect()->route('admin.classes.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }
}
