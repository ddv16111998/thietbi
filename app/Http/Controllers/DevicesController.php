<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Repositories\CategoryRepository;
use App\Repositories\ClassesRepository;
use Illuminate\Http\Request;
use App\Repositories\DeviceRepository;
use App\Repositories\ClassroomRepository;
use App\Repositories\BrokenRepository;
use App\Repositories\BlockRepository;
use App\Repositories\SchoolRepository;
use App\Repositories\SubjectRepository;
use App\Repositories\SchoolYearRepository;

use App\Http\Requests\StoreAddDevicePost;
use App\Http\Requests\StoreEditDevicePost;
use App\Http\Requests\StoreRegistrationBorrowPost;
use App\Http\Requests\StoreAddRepairDevicePost;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Exports\RepairExport;
use Maatwebsite\Excel\Facades\Excel;

class DevicesController extends Controller
{
    protected $repositories;
    protected $stt = 1;

//	protected $path_image = '/upload/images/';

    function __construct(DeviceRepository $deviceRepository)
    {
        $this->repositories = $deviceRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
//            ->editColumn('image', function ($data) {
//                if(file_exists(public_path($data->image)) AND $data->image) $file_img = $data->image;
//                else $file_img = PATH_NO_IMAGE;
//                return '<img src="'.$file_img.'" alt="avatar user" style="height:68px">';
//            }
//             ->editColumn('school_year',function ($data){
//                 return $data->school_year->school_year;
//                })
                ->addColumn('classroom_id', function ($data) {
                    if ($data->classroom) {
                        return implode(', ', $data->classroom->pluck('name')->toArray());
                    } else {
                        return '---';
                    }

                })
                ->addColumn('block_id', function ($data) {
                    return implode(',', $data->block->pluck('name')->toArray());
                })
                ->addColumn('action', function ($data) {
                    return '
                <a href="' . route('admin.deviceup.create', 'device=' . $data->id) . '" class="btn btn-xs btn-success"><i class="fas fa-angle-double-up"></i> Tăng</a>
                <a href="' . route('admin.devicedown.create', 'device=' . $data->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-angle-double-down"></i> Giảm</a>

                <a href="' . route('admin.devices.edit', $data->id) . '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Sửa</a>
                        <a href="javascript:;" data-href="' . route('admin.devices.destroy', $data->id) . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action', 'block_id', 'classroom_id'])
                ->make(true);
        }
        return view('devices.index');
    }


    public function create(SchoolRepository $school, SchoolYearRepository $school_years, CategoryRepository $categoryRepository)
    {
        $data['classrooms'] = $this->repositories->getDataCreate();
        $data['school_years'] = $school_years->getDataIndex();
        $data['categories'] = $categoryRepository->getDataIndex();
        return view('devices.create', $data);
    }

    public function saveDevice($request, $id = null)
    {
        $name = $request->name;
        $code = $request->code;
        $so_hieu = $request->so_hieu;
        $quy_cach = $request->quy_cach;
        $school_year = $request->school_year_id;
        $school_id = $request->school_id;
        $block_id = $request->block_id;
        $classes_id = $request->classes_id;
        $category_id = $request->category_id;
        $classroom_id = $request->classroom_id;
        $unit = $request->unit;
        $amount = $request->amount;
        $price = (float)$request->price;
    	$to_money = $amount * $price;
    	$time_use = Carbon::createFromFormat('d/m/Y', $request->time_use);
    	$year_use = $request->year_use;
    	$description = $request->desciption;

    	$data = [
            'name' => $name,
            'code' => $code,
            'so_hieu' => $so_hieu,
            'quy_cach' => $quy_cach,
            'school_year_id' => $school_year,
            'school_id' => $school_id,
            'block_id' => $block_id,
            'category_id' => $category_id,
            'unit' => $unit,
            'amount' => $amount,
            'price' => $price,
            'into_money' => $to_money,
            'time_use' => $time_use->toDateString(),
            'year_use' => $year_use,
            'description' => $description
        ];

    	$rel_data = [
            'classroom_id' => $classroom_id,
            'classes_id' => $classes_id
        ];
    	return $this->repositories->updateOrCreate($data, $rel_data, $id);
    }

    public function store(StoreAddDevicePost $request)
    {
        $res = $this->saveDevice($request);
        if ($res) {
            return redirect()->route('admin.devices.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        } else {
            return redirect()->route('admin.devices.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }

    public function edit($id, ClassroomRepository $classroom, BlockRepository $block, SchoolRepository $school, ClassesRepository $classes,CategoryRepository $categoryRepository,SchoolYearRepository $schoolYearRepository)
    {
        $data = [];
        $dataDevice = $this->repositories->getDataEdit($id);
        $dataSchool = $school->getDataIndex();
        $schoolSelect = $dataDevice->school()->get();
        $listBlockBySchool = $schoolSelect[0]->block()->get();
        $blockSelect = $dataDevice->block()->get();
        $listClasses = $blockSelect[0]->classes()->get();
        $dataClassroom = $classroom->getDataIndex();
        $data['blocks'] = $block->getDataIndex();
        $data['device'] = $dataDevice;
        $data['classroom'] = $dataClassroom;
        $data['dataSchool'] = $dataSchool;
        $data['listBlockBySchool'] = $listBlockBySchool;
        $data['listClasses'] = $listClasses;
        $data['categories'] = $categoryRepository->getDataIndex();
        $data['school_years'] = $schoolYearRepository->getDataIndex();
        return view('devices.edit', $data);
    }

    public function update(StoreAddDevicePost $request, $id)
    {
        $res = $this->saveDevice($request, $id);
        if ($res) {
            return redirect()->route('admin.devices.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        } else {
            return redirect()->route('admin.devices.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }

    }

    public function destroy($id)
    {
        $delete = $this->repositories->destroy($id);
        if ($delete) {
            return redirect()->route('admin.devices.index')
                ->with(['message' => 'Xóa thành công thiết bị', 'type' => 'success']);
        }
        return redirect()->route('admin.devices.index')
            ->with(['message' => 'Không xóa được thiết bi', 'type' => 'error']);
    }


    /**
     * [ Quan ly muon tra thiet bi ]
     * @param Request
     * @return [ list ]
     */
    public function borrowIndex(Request $request, ClassesRepository $classes)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDecivesBorrow();
            return DataTables::of($data)
                ->editColumn('time_borrow', function ($data) {
                    $cb = new Carbon($data->time_borrow);
                    return $cb->toDateString();
                })
                ->editColumn('time_return', function ($data) {
                    $cb = new Carbon($data->time_return);
                    return $cb->toDateString();
                })
                ->editColumn('device_id', function ($data) {
                    $devices = $data->device()->get();
                    $s = '';
                    foreach ($devices as $device) {
                        $s .= $device->name . '<br>';
                    }
                    return $s;
                })
                ->editColumn('classes_id', function ($data) {
                    return $data->classes->name;
                })
                ->editColumn('status', function ($data) {
                    if ($data->status == 1) return '<b>Đã trả</b>';
                    return '<b>Đang sử dụng</b>';
                })
                ->addColumn('action', function ($data) {
                    return '<a href="' . route('admin.borrow-and-return.edit', $data->id) . '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa / Ghi trả</a>
                        <a href="javascript:;" data-href="' . route('admin.borrow-and-return.destroy', $data->id) . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>
                        <a href="' . route('admin.borrow-and-return.create_fast', $data->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Lập nhanh</a>
                        <a href="' . route('admin.borrow-and-return.create_broken_lost', $data->id) . '" class="btn btn-xs btn-success"><i class="fa fa-edit"></i>Phiếu hỏng mất</a>';
                })
                ->rawColumns(['time_borrow', 'time_return', 'action', 'classes_id', 'device_id', 'status'])
                ->toJson();
        }
        return view('devices.borrow.index');
    }

    public function createBrokenLost($id)
    {
        $brokenLost = $this->repositories->getDataBorrowById($id);
        $devices = $this->repositories->getDataIndex();
        $data['deviceOne'] = $brokenLost->device()->get();
        $data['devices'] = $devices;
        return view('broken.create', $data);
    }

    public function registrationBorrow(SubjectRepository $sub, SchoolYearRepository $school_year)
    {
        $data = [];
        $devices = $this->repositories->getDataIndex();
        $subjects = $sub->getDataIndex();
        $school_years = $school_year->getDataIndex();
        $data['devices'] = $devices;
        $data['subjects'] = $subjects;
        $data['school_years'] = $school_years;
        return view('devices.borrow.create', $data);
    }

    public function createFast($id, SubjectRepository $sub, SchoolYearRepository $school_year)
    {
        $borrow = $this->repositories->getDataBorrowById($id);
        $devicesData = $borrow->device()->get();
        $devices = $this->repositories->getDataIndex();
        $subjects = $sub->getDataIndex();
        $school_years = $school_year->getDataIndex();
        $data['devicesData'] = $devicesData;
        $data['devices'] = $devices;
        $data['subjects'] = $subjects;
        $data['school_years'] = $school_years;
        return view('devices.borrow.create', $data);
    }

    public function saveDeviceBorrow($request, $id = null)
    {
        $time_borrow = Carbon::createFromFormat('d/m/Y', $request->time_borrow);
        $time_return = Carbon::createFromFormat('d/m/Y', $request->time_return);
        $teacher = $request->teacher;
        $device_id = $request->device_id;
        $amount = $request->amount;
        $subject_id = $request->subject_id;
        $school_year_id = $request->school_year_id;
        $mass = $request->mass;
        $school_id = $request->school_id;
        $block_id = $request->block_id;
        $classes_id = $request->classes_id;
        $note = strip_tags($request->note);
        $lesson_name = $request->lesson_name;
        $lesson_num = json_encode($request->lesson_num);
        $lesson_use = $request->lesson_use;
        $is_practice = ($request->is_practice) ? 1 : 0;
        $return_date = ($request->return_date) ? Carbon::createFromFormat('d/m/Y', $request->return_date)->toDateString() : null;
//        dd($return_date);ok
        $status = ($request->status) ? (int)$request->status : 0;
//        var_dump($status);ok
        $state = ($request->state) ? $request->state : null;
//        dd($state);
        $data = [
            'time_borrow' => $time_borrow->toDateString(),
            'time_return' => $time_return->toDateString(),
            'teacher' => $teacher,
            'discipline' => $subject_id,
            'school_year_id' => $school_year_id,
            'mass' => $mass,
            'school_id' => $school_id,
            'block_id' => $block_id,
            'classes_id' => $classes_id,
            'note' => $note,
            'lesson_name' => $lesson_name,
            'lesson_num' => $lesson_num,
            'lesson_use' => $lesson_use,
            'is_practice' => $is_practice,
            'return_date' => $return_date,
            'status' => $status,
            'state' => $state

        ];
        $param = [
            'device_id' => $device_id,
            'amount' => $amount,
        ];

        return $this->repositories->updateOrCreateBorrow($data, $param, $id);
    }

    public function handleBorrow(StoreRegistrationBorrowPost $request)
    {
        $result = $this->saveDeviceBorrow($request);
        if ($result) {
            return redirect()->route('admin.borrow-and-return.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        return redirect()->route('admin.borrow-and-return.index')
            ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
    }

    public function editBorrow($id, SubjectRepository $sub, SchoolYearRepository $school_year)
    {
        $data = [];
        $devices = $this->repositories->getDataIndex();
        $deviceBorrow = $this->repositories->getDataBorrowById($id);
        // dd($deviceBorrow);
        // $devices_id = $deviceBorrow->device()->get();
        // $array_device_id = [];
        // foreach ($devices_id as $device_id)
        // {
        //    array_push($array_device_id,$device_id->pivot->device_id);
        // }
        $deviceBorrow['lesson_num'] = json_decode($deviceBorrow['lesson_num'], true);
        $subjects = $sub->getDataIndex();
        $school_years = $school_year->getDataIndex();
        $data['deviceBorrow'] = $deviceBorrow;
        $data['devices'] = $devices;
        $data['subjects'] = $subjects;
        $data['school_years'] = $school_years;
        // $data['array_devices_id'] = $array_device_id;
        return view('devices.borrow.edit', $data);
    }


    public function updateBorrow(StoreRegistrationBorrowPost $request, $id)
    {
        $result = $this->saveDeviceBorrow($request, $id);
        if ($result) {
            return redirect()->route('admin.borrow-and-return.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        } else {
            return redirect()->route('admin.borrow-and-return.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }

    public function destroyBorrow($id)
    {
        $delete = $this->repositories->destroyBorrow($id);
        if ($delete) {
            return redirect()->route('admin.borrow-and-return.index')
                ->with(['message' => 'Đã trả thiết bị', 'type' => 'success']);
        }
        return redirect()->route('admin.borrow-and-return.index')
            ->with(['message' => 'Chưa trả thành công thiết bi', 'type' => 'error']);
    }


    // controller repair (sữa chữa thiết bị) device
    public function repairIndex(Request $request)
    {
        if ($request->ajax()) {
            if (isset($request->id)) {
                return $request->id;
            }
            $data = $this->repositories->getDataRepairIndex();
            return Datatables::of($data)
                ->addColumn('stt', function ($data) {
                    return $this->stt++;
                })
                ->editColumn('broken_id', function ($data) {
                    return $data->broken->name;
                })
                ->editColumn('school_year_id', function ($data) {
                    return $data->school_year->school_year;
                })
                ->addColumn('device', function ($data) {
                    $dv = $data->device()->get();
                    $ss = null;
                    foreach ($dv as $v) {
                        $ss .= $v->name . '<br>';
                    }
                    return $ss;
                })
                ->editColumn('desciption', function ($data) {
                    return strip_tags($data->desciption);
                })
                ->addColumn('action', function ($data) {
                    $string = '
                          <a href="' . route('admin.repair.edit', $data->id) . '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                          <a href="javascript:void(0)" data-href="' . route('admin.repair.destroy', $data->id) . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                          <a href="' . route('admin.repair.follow', $data->id) . '" class="btn btn-xs btn-crud-primary"><i class="fa fa-eye"></i>Theo dõi</a>
                          ';
                    return $string;
                })
                ->rawColumns(['broken_id', 'device', 'desciption', 'action'])
                ->toJson();
        }
        return view('devices.repair.index');
    }

    public function repairCreate(Request $request, BrokenRepository $broken, SchoolYearRepository $school_year)
    {
        if ($request->ajax()) {
            $dv = $broken->getById($request->broken_id)->brokenDevice()->get();
            $data = '<label>Thiết bị cần sửa chữa</label>';
            foreach ($dv as $val) {
                $id = $val->device_id;
                $deviceData = $this->repositories->getDataDeviceById($id);
                $data .= '
                            <div class="row form-group">
                                <div class="col-4">
                                    <select class="device-ajax form-control" name="device_id[]">
                                        <option value="' . $val->device_id . '">' . $deviceData['name'] . '</option>
                                    </select>
                                 </div>
                                <div class="col-4">
                                    <input type="number" name="amount[]" class="form-control" value="' . $val->amount . '" placeholder="Số lượng"/>
                                </div>
                                <div class="col-3">
                                    <input type="text" name="price[]" class="form-control" placeholder="Giá tiền sửa/thiết bị"/>
                                </div>
                            </div>
                        ';
            }
            return $data;
        }
        $brokens = $broken->getDataIndex();
        $school_years = $school_year->getDataIndex();
        $data['brokens'] = $brokens;
        $data['school_years'] = $school_years;
        return view('devices.repair.create', $data);
    }

    public function saveRepairDevice(Request $request, $id)
    {
        $ballot = $request->ballot;
        $start_day = Carbon::createFromFormat('d/m/Y', $request->start_day);
        $school_year_id = $request->school_year_id;
        $broken_id = $request->broken_id;
        $device_id = $request->device_id;
        $amount = $request->amount;
        $price = ($request->price) ? $request->price : 0;
        $state = $request->state;
        $desciption = strip_tags($request->desciption);
        $data = [
            'ballot' => $ballot,
            'start_day' => $start_day->toDateString(),
            'school_year_id' => $school_year_id,
            'broken_id' => $broken_id,
            'state' => $state,
            'desciption' => $desciption,
        ];
        $rel_data = [
            'device_id' => $device_id,
            'amount' => $amount,
            'price' => $price,
        ];
        return $this->repositories->updateOrCreateRepair($data, $rel_data, $id);
    }

    public function repairStore(StoreAddRepairDevicePost $request)
    {
        $result = $this->saveRepairDevice($request, $id = null);
        if ($result) {
            return redirect()->route('admin.repair.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        } else {
            return redirect()->route('admin.repair.index')
                ->with(['message' => ' mới thất bại', 'type' => 'error']);
        }
    }

    public function repairEdit($id, BrokenRepository $broken, Request $request, SchoolYearRepository $school_year)
    {
        if ($request->ajax()) {
            $dv = $broken->getById($request->broken_id)->brokenDevice()->get();
            $data = '<label>Thiết bị cần sửa chữa</label>';
            foreach ($dv as $val) {
                $id = $val->device_id;
                $deviceData = $this->repositories->getDataDeviceById($id);
                $data .= '
                            <div class="row form-group">
                                <div class="col-4">
                                    <select class="device-ajax form-control" name="device_id[]">
                                        <option value="' . $val->device_id . '">' . $deviceData['name'] . '</option>
                                    </select>
                                 </div>
                                <div class="col-4">
                                    <input type="number" name="amount[]" class="form-control" value="' . $val->amount . '" placeholder="Số lượng"/>
                                </div>
                                <div class="col-3">
                                    <input type="text" name="price[]" class="form-control" placeholder="Giá tiền sửa/thiết bị" value=""/>
                                </div>
                            </div>
                        ';
            }
            return $data;
        }
        $repair = $this->repositories->getDataRepairById($id);
        $devices = $repair->device()->get();
        $brokens = $broken->getDataIndex();
        $school_years = $school_year->getDataIndex();
        $data['repair'] = $repair;
        $data['brokens'] = $brokens;
        $data['devices'] = $devices;
        $data['school_years'] = $school_years;
        return view('devices.repair.edit', $data);
    }

    public function repairUpdate(StoreAddRepairDevicePost $request, $id)
    {
        $result = $this->saveRepairDevice($request, $id);
        if ($result) {
            return redirect()->route('admin.repair.index')
                ->with(['message' => 'Thay đổi thành công', 'type' => 'success']);
        } else {
            return redirect()->route('admin.repair.index')
                ->with(['message' => 'Thay đổi thất bại', 'type' => 'error']);
        }
    }

    public function repairDestroy($id)
    {
        $delete = $this->repositories->destroyRepair($id);
        if ($delete) {
            return redirect()->route('admin.repair.index')
                ->with(['message' => 'Đã sửa xong thiết bị', 'type' => 'success']);
        }
        return redirect()->route('admin.repair.index')
            ->with(['message' => 'Chưa sửa xong thiết bi', 'type' => 'error']);
    }

    public function repairFollow($id, Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->Broken_Repair_By_RepairId($id);
            return Datatables::of($data)
                ->addColumn('stt', function ($data) {
                    return $this->stt++;
                })
                ->editColumn('repair_id', function ($data) {
                    return $data->device_repair->ballot;
                })
                ->editColumn('device_id', function ($data) {
                    return $this->repositories->getDataDeviceById($data->device_id)->name;
//                    return $data->device; ?? khong tham chieu duoc
                })
                ->addColumn('total_price', function ($data) {
                    return number_format(($data->amount) * ($data->price));
                })
                ->editColumn('status', function ($data) {
                    if ($data->status == 0) return '<a href="#" class="btn btn-xs btn-warning"><i class="fa fa-chain-broken"></i> Cần sửa chữa</a>';
                    if ($data->status == 1) return '<a href="#" class="btn btn-xs btn-primary"><i class="fa fa-wrench "></i> Đang sửa chữa</a>';
                    if ($data->status == 3) return '<a href="#" class="btn btn-xs btn-danger"><i class="fas fa-exclamation-triangle"></i> Không sửa chữa được</a>';
                    if ($data->status == 2) return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Đã sửa chữa xong</a>';
                })
                ->addColumn('update_status', function ($data) {
                    return '<a href="javascript:void(0)" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#updateStatus" onclick="updateStatus(' . $data->id . ')"><i class="fa"></i> Thay đổi trạng thái sữa chữa</a>';
                })
                ->rawColumns(['repair_id', 'device_id', 'total_price', 'status', 'update_status'])
                ->toJson();
        }
        return view('devices.repair.follow', ['id' => $id]);
    }

    public function changeStatus($id, $status, DeviceRepository $device)
    {
        $data = $this->repositories->Broken_Repair_By_Id($id);
        $id_ballot = $data->repair_id;
        $data->status = (int)$status;
        $device_id = $data->device_id;
        $deviceOne = $device->getDataEdit($device_id);
        $amount = $deviceOne->amount;
        if ($data->status == 2) {
            $status = $data->status;
            Device::updateOrCreate(['id' => $device_id], ['amount' => $data->amount + $amount]);
        } else {
            if (isset($status)) {
                Device::updateOrCreate(['id' => $device_id], ['amount' => $amount - $data->amount]);
            }
        }
        $data->save();
        return redirect()->route('admin.repair.follow', ['id' => $id_ballot]);
    }

    public function trackBook(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->AllDataBroken_Repair();
            return Datatables::of($data)
                ->addColumn('stt', function ($data) {
                    return $this->stt++;
                })
                ->editColumn('repair_id', function ($data) {
                    return $data->device_repair->ballot;
                })
                ->addColumn('start_day', function ($data) {
                    return $data->device_repair->start_day;
                })
                ->editColumn('device_id', function ($data) {
                    return $this->repositories->getDataDeviceById($data->device_id)->name;
//                    return $data->device->name;
                })
                ->addColumn('total_price', function ($data) {
                    return number_format(($data->amount) * ($data->price));
                })
                ->toJson();
        }

        return view('devices.repair.track_book');
    }

    public function exportExcel()
    {
        return Excel::download(new RepairExport, 'repairs.xlsx');
    }
}
