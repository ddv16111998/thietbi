<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DiarySystemRepository;
use Yajra\Datatables\Datatables;

class DiarySystemController extends Controller
{
	function __construct(DiarySystemRepository $diarySystemRepository)
    {
        $this->repositories = $diarySystemRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
            ->editColumn('user_id', function ($data) {
                return $data->user->first()->username;
            })
            ->editColumn('created_at', function ($data) {
               return date_format($data->created_at, 'H:i:s - d/m/Y');
            })
            ->rawColumns(['action','avatar'])
            ->make(true);
        }
    	return view('diary.index');
    }
}
