<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserThemeRepository;
use App\Repositories\CityRepository;
use App\Repositories\SchoolRepository;

class HomeController extends Controller
{
    public function index()
    {
    	return view('dashboard.index');
    }
    public function changeTheme($change)
    {
    	UserThemeRepository::changeTheme($change);
    	return back()->with(['message' => 'Thay đổi giao diện '.$change.' thành công', 'type' => 'success']);
    }

    public function get_location(CityRepository $city, Request $request)
    {
        if($request->ajax()){
            if($request->city_check){
                $string = '<option selected>--None--</option>';
                foreach ($city->getAllCity() as $value) {
                    $string .= '<option value="'.$value->matp.'">'.$value->name.'</option>';
                }
                return $string;
            }
            if(isset($request->maqh)){
                $string = '<option>--None--</option>';
                foreach ($city->getCommuneByDistrict($request->maqh) as $value) {
                    $string .= '<option value="'.$value->xaid.'">'.$value->name.'</option>';
                }
                return $string;
            }else{
                $string = '<option>--None--</option>';
                foreach ($city->getDistrictByCity($request->matp) as $value) {
                    $string .= '<option value="'.$value->maqh.'">'.$value->name.'</option>';
                }
                return $string;
            }
        }
    }
    public function get_info(SchoolRepository $school,Request $request)
    {
        if($request->ajax())
        {
            if($request->school_check){
                $string = '<option>-- Chọn trường học --</option>';
                foreach ($school->getDataIndex() as $value) {
                    $string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $string;
            }
            if(isset($request->block_id)){
                $string = '<option>--None--</option>';
                foreach ($school->getClassByBlock($request->block_id) as $value) {
                    $string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $string;
            }
            else{
                $string = '<option>--None--</option>';
                foreach ($school->getBlockBySchool($request->school_id) as $value) {
                    $string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $string;
            }
        }
    }
}
