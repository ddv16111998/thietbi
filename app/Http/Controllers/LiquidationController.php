<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Repositories\SchoolYearRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\LiquidationRepository;
use App\Repositories\BrokenDeviceRepository;

use App\Http\Requests\StoreAddLiquidationPost;
use Maatwebsite\Excel\Facades\Excel;
class LiquidationController extends Controller
{
    protected $repository;

    function __construct(LiquidationRepository $liquidation)
    {
        $this->repository = $liquidation;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('device',function ($data){
                    $dv = $data->device()->get();
                    $ss = null;
                    foreach ($dv as $val)
                    {
                        $ss .= $val->name.' ('.$val->pivot->amount.' '.$val->unit.')<br>';
                    }
                    return $ss;
                })
                ->addColumn('action', function ($data) {
                    $string = '
                  <a href="'.route('admin.liquidation.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                  <a href="javascript:void(0)" data-href="'.route('admin.liquidation.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                  ';
                            return $string;
                        })
                ->rawColumns(['action', 'device'])
                ->toJson();
        }
        return view('liquidation.index');
    }
    public function create(SchoolYearRepository $school_year,DeviceRepository $device,Request $request,BrokenDeviceRepository $brDevice)
    {
        if($request->ajax()){
            if($request->device_name)
            {
                $param = [
                    ['name', 'like', '%'.$request->device_name.'%'],
                ];
                $devices = $this->repository->getDataSearch($param);
                $string = '';
                foreach ($devices as $device_one) {
                    $string .= '<tr>
                    <td>'.$device_one->so_hieu.'</td>
                    <td>'.$device_one->code.'</td>
                    <td>'.$device_one->name.'</td>
                    <td>'.$device_one->unit.'</td>
                    <td><button class="btn btn-info select_device" value="'.$device_one->id.'" data-device="'.$device_one->name.'" title="chọn">>>></button></td>
                </tr>';
                }
                return $string;
            }
            if($request->typeData == 0)
            {
                $brokenDevices = $brDevice->getIndex();
                dd($brokenDevices);
                $string ='';
                foreach ($brokenDevices as $brokneDevive)
                {
                    $string .= '<tr>
                <td>'.$brokneDevive->device->so_hieu.'</td>
                <td>'.$brokneDevive->device->code.'</td>
                <td>'.$brokneDevive->device->name.'</td>
                <td>'.$brokneDevive->device->unit.'</td>
                <td><button class="btn btn-info select_device" value="'.$brokneDevive['device_id'].'" data-device="'.$brokneDevive->device->name.'" title="chọn">>>></button></td>
            </tr>';
                }
                return $string;
            }
            if($request->typeData == 1)
            {
                $devices = $device->getDataIndex();
                $string = '';
                foreach ($devices as $deviceOne)
                {
                    if($deviceOne->amount > 0)
                    {
                        $string .= '<tr>
                        <td>'.$deviceOne->so_hieu.'</td>
                        <td>'.$deviceOne->code.'</td>
                        <td>'.$deviceOne->name.'</td>
                        <td>'.$deviceOne->unit.'</td>
                        <td><button class="btn btn-info select_device" value="'.$deviceOne->id.'" data-device="'.$deviceOne->name.'" title="chọn">>>></button></td>
                    </tr>';
                    }

                }
                return $string;
            }

        }
        $school_years = $school_year->getDataIndex();
        $devices      = $device->getDataIndex();
        $data['school_years'] = $school_years;
        $data['devices'] = $devices;
        return  view('liquidation.create',$data);
    }

    public function save(Request $request,$id)
    {
        $name = $request->name;
        $school_year_id = $request->school_year_id;
        $create_date = Carbon::createFromFormat('d/m/Y', $request->create_date);
        $council = $request->council;
        $description = $request->description;
        $device_id = $request->device_id;
        $amount = $request->amount;
        $note = $request->note;

        $data = [
          'name' => $name,
          'school_year_id' => $school_year_id,
          'create_date'    => $create_date->toDateString(),
          'council'        => $council,
          'description'    => $description,
        ];
        $param = [
            'device_id' => $device_id,
            'amount'    => $amount,
            'note'      => $note
        ];

        return $this->repository->updateOrCreate($data,$param,$id);
    }

    public function store(StoreAddLiquidationPost $request,$id=null)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.liquidation.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
        }
        return redirect()->route('admin.liquidation.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
    }

    public function edit($id,SchoolYearRepository $school_year,DeviceRepository $device)
    {
        $school_years = $school_year->getDataIndex();
        $devices      = $device->getDataIndex();
        $liquidation = $this->repository->getDataEdit($id);
        $liquidation_device = $liquidation->device()->get();
        $data['school_years'] = $school_years;
        $data['devices'] = $devices;
        $data['liquidation'] = $liquidation;
        $data['liquidation_device'] = $liquidation_device;
        return view('liquidation.edit',$data);
    }
    public function update($id,StoreAddLiquidationPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.liquidation.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        }
        return redirect()->route('admin.liquidation.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
    }

    public function destroy($id)
    {
            $data = $this->repository->destroy($id);
            if($data)
            {
                return redirect()->route('admin.liquidation.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
            }
            return redirect()->route('admin.liquidation.index')->with(['message'=>'Xóa thất bại', 'type'=>'error']);
    }
}
