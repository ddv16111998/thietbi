<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCreateRole;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Repositories\RoleRepository;

class RoleController extends Controller
{
	protected $repositories;

    function __construct(RoleRepository $roleRepository)
    {
        $this->repositories = $roleRepository;
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
                ->addColumn('status', function ($data) {
                if($data->status == 1) return '<code class="text-success">Hoạt động</code>';
                if($data->status == 0) return '<code class="text-danger">Ẩn</code>';
            })
            ->addColumn('action', function ($data) {
                $string = '
                    <a href="'.route('admin.role.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                    <a href="javascript:void(0)" data-href="'.route('admin.role.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                ';
                return $string;
            })
            ->rawColumns(['status', 'action'])->toJson();
        }
        return view('role.index');
    }
    public function create()
    {
        $route_name = $this->repositories->getDataCreate();
        return view('role.create', compact('route_name'));
    }
    public function saveRole($request, $id = null)
    {
        $data['name']   = $request->name;
        $data['permissions']  = implode('|', $request->permisson);
        return $this->repositories->updateOrCreate($data,$id);
    }
    public function store(Request $request)
    {
        $res = $this->saveRole($request);
        if ($res) {
            return redirect()->route('admin.role.index')
                    ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->back()
                    ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }
    public function edit($id)
    {
        $data = $this->repositories->getDataEdit($id);
        return view('role.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $res = $this->saveRole($request, $id);
        if ($res) {
            return redirect()->route('admin.role.index')
                    ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->back()
                    ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }
    public function destroy($id)
    {
      $this->repositories->destroy($id);
      return redirect()->route('admin.role.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }
}
