<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\StoreCreateSchoolPost;
use App\Repositories\SchoolYearRepository;
use Yajra\Datatables\Datatables;
use App\Repositories\SchoolRepository;
use App\Repositories\School_detailRepository;

class SchoolController extends Controller
{
    protected $repositories;
    protected $repositories_detail;
    protected $path_image = '/upload/images/';

    function __construct(SchoolRepository $schoolRepository, School_detailRepository $school_detailRepository)
    {
        $this->repositories = $schoolRepository;
        $this->repositories_detail = $school_detailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
          if (isset($request->id)) {
            $data = $this->repositories->getById($request->id);
            $obj = ['school'=>$data, 'school_detail'=>$data->school_detail];
            return $obj;
          }
          $data = $this->repositories->getDataIndex();
          return DataTables::of($data)
          ->addColumn('year_link', function ($data) {
            return $data->school_detail['year_link'];
          })
          ->addColumn('total_class', function ($data) {
            return $data->school_detail['total_class'];
          })
          ->addColumn('level', function ($data) {
            return $data->school_detail['level'];
          })
          ->addColumn('principal_birthday', function ($data) {
            return $data->school_detail['principal_birthday'];
          })
          ->addColumn('principal_email', function ($data) {
            return $data->school_detail['principal_email'];
          })
          ->addColumn('principal_phone', function ($data) {
            return $data->school_detail['principal_phone'];
          })
          ->addColumn('vice_principal_name', function ($data) {
            return $data->school_detail['vice_principal_name'];
          })
          ->addColumn('vice_principal_birthday', function ($data) {
            return $data->school_detail['vice_principal_birthday'];
          })
          ->addColumn('vice_principal_email', function ($data) {
            return $data->school_detail['vice_principal_email'];
          })
          ->addColumn('vice_principal_phone', function ($data) {
            return $data->school_detail['vice_principal_phone'];
          })
          ->addColumn('action', function ($data) {
            $string = '
            <a href="javascript:void(0)" class="btn btn-xs btn-info detailAjax" data-toggle="modal" data-target="#detailSchool" onClick="getDetailSchool('.$data->id.')"><i class="fas fa-eye"></i>Chi tiết</a>
            <a href="'.route('admin.school.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
            <a href="javascript:void(0)" data-href="'.route('admin.school.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
            ';
            return $string;
          })
          ->rawColumns(['action'])
          ->toJson();
      }
      return view('school.index');
    }

    public function getDetail(Request $request)
    {
      dd($request);

      return "dfjk";
    }

    public function saveData($request, $id = null)
    {
      $school = [
        'name' => $request->name,
        'city' => $request->city,
        'district' => $request->district,
        'commune' => $request->commune,
        'address' => $request->address,
        'email' => $request->email,
        'hotline' => $request->hotline,
        'principal_name' => $request->principal_name,
      ];
      $school_id = $this->repositories->updateOrCreate($school, $id);
      $principal_birthday=  Carbon::createFromFormat('d/m/Y', $request->principal_birthday);
      $vice_principal_birthday= Carbon::createFromFormat('d/m/Y', $request->vice_principal_birthday);
      $school_detail = [
        'school_id' => $school_id,
        'level' => $request->level,
        'year_link' => $request->year_link,
        'total_class' => $request->total_class,
        'principal_birthday' => $principal_birthday->toDateString(),
        'principal_phone' => $request->principal_phone,
        'principal_email' => $request->principal_email,
        'vice_principal_name' => $request->vice_principal_name,
        'vice_principal_birthday' => $vice_principal_birthday->toDateString(),
        'vice_principal_phone' => $request->vice_principal_phone,
        'vice_principal_email' => $request->vice_principal_email,
      ];
      // dd($request);
      if($request->hasFile('image_maps'))
      {
        $photoMaps = $request->file('image_maps');
        $image_maps  = $photoMaps->getClientOriginalName();
        $photoMaps->move('upload/images',$image_maps);
        $school_detail['image_maps'] = $this->path_image.$image_maps;
      }
      if($request->hasFile('image_schedule'))
      {
        $photoSchedule = $request->file('image_schedule');
        $image_schedule  = $photoSchedule->getClientOriginalName();
        $photoSchedule->move('upload/images',$image_schedule);
        $school_detail['image_schedule'] = $this->path_image.$image_schedule;
      }
      $this->repositories_detail->updateOrCreate($school_detail, $school_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SchoolYearRepository $school_year)
    {
      return view('school.create', ['school_years'=>$school_year->getDataIndex()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCreateSchoolPost $request)
    {
      $this->saveData($request);
      return redirect()->route('admin.school.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SchoolYearRepository $school_year)
    {
        $school = $this->repositories->getById($id);
        // dd($school->school_detail());

        return view('school.edit', ['school_years'=>$school_year->getDataIndex(), 'school'=>$school]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCreateSchoolPost $request)
    {
      $this->saveData($request, $request->id);
      return redirect()->route('admin.school.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $this->repositories->destroy($id);
      return redirect()->route('admin.school.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }
}
