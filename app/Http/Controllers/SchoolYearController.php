<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SchoolYearRepository;
use App\Http\Requests\StoreAddSchoolYearPost;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;
class SchoolYearController extends Controller
{
    protected $repositories;
    private $stt =1;
    protected $path_image = '/upload/images/';

    function __construct(SchoolYearRepository $school_year)
    {
        $this->repositories = $school_year;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repositories->getDataIndex();
            return Datatables::of($data)
                ->addColumn('stt',function ($data){
                    return $this->stt++;
                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.school-year.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Sửa</a>
                        <a href="javascript:;" data-href="'.route('admin.school-year.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['stt','action'])
                ->toJson();
        }
        return view('school-year.index');
    }
    public function create()
    {
        return view('school-year.create');
    }
    public function save(Request $request,$id)
    {
        $start_year = $request->start;
        $end_year   = $request->end;
        $start_HK1  = Carbon::createFromFormat('d/m/Y', $request->start_HK1);
        $start_HK2  = Carbon::createFromFormat('d/m/Y', $request->start_HK2);
        $end_day_year   = Carbon::createFromFormat('d/m/Y', $request->end_year);

        $data =[
            'school_year' => $start_year." - ".$end_year,
            'start_HK1'   => $start_HK1->toDateString(),
            'start_HK2'   => $start_HK2->toDateString(),
            'end_year'    => $end_day_year->toDateString(),
        ];
        return $this->repositories->updateOrCreate($data,$id);

    }
    public function store(StoreAddSchoolYearPost $request)
    {
        $data = $this->save($request,$id=null);
        if($data)
        {
            return redirect()->route('admin.school-year.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.school-year.index')
                ->with(['message' => ' Thêm mới thất bại', 'type' => 'error']);
        }
    }
    public function edit($id)
    {
        $data = $this->repositories->getDataEdit($id);
        return view('school-year.edit',['data'=>$data]);
    }

    public function update($id,StoreAddSchoolYearPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.school-year.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.school-year.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }
    public function destroy($id)
    {
        $data = $this->repositories->destroy($id);
        if($data)
        {
            return redirect()->route('admin.school-year.index')
                ->with(['message' => 'Xóa thành công năm học', 'type' => 'success']);
        }
        return redirect()->route('admin.school-year.index')
            ->with(['message' => 'Không xóa được năm học', 'type' => 'error']);
    }
}
