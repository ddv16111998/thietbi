<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SystemController extends Controller
{
    public function show_log()
    {
    	$logs = '';
    	$file_log = storage_path('logs/laravel-'.date('Y-m-d').'.log');
    	if (file_exists($file_log)) {
    		$logs = file_get_contents($file_log);
    		if (preg_match('/\[\d{4}-\d{2}-\d{2}( )\d{2}:\d{2}:\d{2}\]( )(\S)*(ERROR:)/', $logs, $match)) {
    			dump($match);
    			// str_replace($match[1], '<span style="color:#f33">'.$match[1].'</span>', $logs);
    			// // // $match = '<span style="color:#f33">'.$match.'</span>';
    		}
    		echo "<pre>".$logs."</pre>";
    	}
    	die;
    }
}
