<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAddUserPost;
use App\Http\Requests\StoreEditUserPost;
use App\Repositories\UserRepository;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class UserController extends Controller
{
    protected $repositories;
    protected $path_image = '/upload/images/';

    function __construct(UserRepository $userRepository)
    {
        $this->repositories = $userRepository;
    }

    public function index(Request $request)
    {
        $data_role = $this->repositories->getDataRole();
        if ($request->ajax()) {
            return $this->getDataUsers();
        }
        return view('user.index', compact('data_role'));
    }

    public function getDataUsers()
    {
        $data = $this->repositories->getDataUsers();
        return DataTables::of($data)
        ->editColumn('avatar', function ($data) {
            if(file_exists(public_path($data->avatar)) AND $data->avatar) $file_img = $data->avatar;
            else $file_img = PATH_NO_IMAGE;
            return '<img src="'.$file_img.'" alt="avatar user" style="height:68px">';
        })
        ->addColumn('role', function ($data) {
            if ($data->role_id) {
                return $data->role->find($data->role_id)->name;
            }
            else return 'Khách';
        })
        ->addColumn('status', function ($data) {
            return config('data.user.status.'.$data->status);
        })
        ->addColumn('action', function ($data) {
            return '<a href="'.route('admin.user.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Edit</a>
                    <a href="javascript:;" data-href="'.route('admin.user.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>
                    <a href="javascript:;" data-id="'.$data->id.'" data-role="'.$data->role_id.'" class="btn__role btn btn-xs btn-info" data-toggle="modal" data-target="#selectRole"><i class="ti-shield"></i> Vai Trò </a>';
        })
        ->rawColumns(['action','avatar'])
        ->make(true);
    }

    public function create()
    {
        return view('user.create');
    }

    public function saveUser($request, $id = null)
    {
        $username   = $request->name;
        $email      = $request->email;
        $password   = $request->password;
        $acc        = $request->acc;

        $param = [
            'department' => $request->department,
            'position'   => $request->position
        ];

        if ($request->status) $status = $request->status;
        else $status = 1;
        $data = [
            'username'  => $username,
            'email'     => $email,
            'acc'       => $acc,
            'password'  => bcrypt($password),
            'status'    => $status
        ];
        if($request->hasFile('avatar'))
        {
            $photoFile = $request->file('avatar');
            $nameFile  = $photoFile->getClientOriginalName();
            if($nameFile)
            {
                $avatar = $nameFile;
                $photoFile->move('upload/images',$nameFile);
            }
            $data['avatar']  = $this->path_image.$avatar;
        }
        return $this->repositories->updateOrCreate($data,$param,$id);
    }

    public function store(StoreAddUserPost $request)
    {
        $res = $this->saveUser($request);
        if ($res) {
            return redirect()->route('admin.user.index')->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->back()->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }

    public function selectRole(Request $request)
    {
        $id = $request->user_id;
        $data['role_id'] = (int) $request->role;
        $res = $this->repositories->updateOrCreate($data,$param = null,$id);
        if ($res) {
            return redirect()->back()->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->back()->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }

    public function edit($id)
    {
        $data = $this->repositories->getDataEdit($id);
        return view('user.edit',$data);
    }

    public function update(StoreEditUserPost $request, $id)
    {
        $res = $this->saveUser($request, $id);
        if ($res) {
            return redirect()->route('admin.user.index')->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->back()->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }

    }

    public function destroy($id)
    {
        $delete = $this->repositories->destroy($id);
        if($delete)
        {
            return redirect()->route('admin.user.index')->with(['message' => 'Xóa thành công tài khoản người dùng', 'type' => 'success']);
        }
        return redirect()->back()->with(['message' => 'Không xóa được tài khoản người dùng', 'type' => 'error']);
    }
}
