<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Role;

class PermissionUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Request::isMethod("GET")) {
            $role_id    = \Auth::user()->role_id;
            $role_user  = Role::select('permissions')->find($role_id);
            $permissions = explode('|', $role_user->permissions);   // dd($permissions);
            $routeCollection = \Route::currentRouteName();  // dd($routeCollection);
            if (in_array($routeCollection, $permissions)) return $next($request);
            else return redirect()->route('admin.dashboard')->with(['message' => 'Bạn không được cấp quyền truy cập chức năng này', 'type' => 'error']);
        }
        else return $next($request);
    }
}
