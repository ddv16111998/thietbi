<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddDevicePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required',
            'so_hieu' =>'required',
            'quy_cach'   => 'required',
            'school_year_id'=>'required',
            'school_id' => 'required',
            'block_id' => 'required',
            'classes_id' => 'required',
            'classroom_id'=> 'required',
            'unit' => 'required',
            'amount'=> 'required',
            'price'=> 'required',
            'time_use' => 'required',
            'year_use' => 'required',
            'desciption' => 'required'

        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Bắt buộc phải nhập tên thiết bị',
            'so_hieu.required'=>'Bắt buộc phải nhập số hiệu.',
            'quy_cach.required' =>'Bắt buộc phải nhập quy cách',
            'school_year_id.required'=> 'Bắt buộc phải chọn năm học',
            'school_id.required' => 'Bắt buộc phải chọn trường',
            'block_id.required' => 'Bắt buộc phải chọn khối',
            'classes_id.required' => 'Bắt buộc phải chọn lớp',
            'classroom_id.required'=>' Bắt buộc phải chọn phòng học',
            'unit.required'=>' Bắt buộc phải nhập đơn vị',
            'amount.required'=>' Bắt buộc phải nhập số lượng',
            'price.required'=>' Bắt buộc phải nhập giá',
            'time_use.required'=>' Bắt buộc phải chọn ngày đưa vào sử dụng',
            'year_use.required'=>' Bắt buộc phải chọn năm sử dụng',
            'desciption.required'=>' Bắt buộc phải nhập mô tả',
        ];
    }
}
