<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddLiquidationPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'school_year_id'=> 'required',
            'create_date'   => 'required',
            'council'       => 'required',
            'description'   => 'required',
            'amount'        => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Số phiếu không được để trống',
            'school_year_id.required' => 'Năm học không được để trống',
            'create_date.required'      => 'Ngày lập không được để trống',
            'council.required'          => 'Hội đồng thanh lý không được để trống',
            'description.required'      => 'Nội dung không được để trống',
            'amount'                    => 'Số lượng thiết bị thanh lý không được để trống',
        ];
    }
}
