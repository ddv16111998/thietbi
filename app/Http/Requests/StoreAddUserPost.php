<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class StoreAddUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'  =>'required',
            'email' =>'required|email|unique:users,email',
            'acc'   =>'required|alpha_dash|unique:users,acc',
            'password'  =>'required|min:3|max:20',
            'password_verified'=>'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Tên không được để trống !',
            'email.required'=>'Email không được để trống !',
            'email.email'   =>'Email phải đúng định dạng .',
            'email.unique'  =>'Email đã tồn tại. Vui lòng chọn email khác !',
            'acc.unique'    =>'Tài khoản đã tồn tại. Vui lòng chọn tài khoản khác !',
            'acc.alpha_dash' =>'Tài khoản không được chứa khoảng trắng',
            'password.required'     =>'Mật khẩu không được để trống !',
            'password_verified.required'    =>'Vui lòng nhập lại mật khẩu !',
            'password.min'  =>'Mật khẩu chứa ít nhất :min kí tự !',
            'password.max'  =>'Mật khẩu chứa nhiều nhất :max kí tự !',
            'password_verified.same'    =>'Mật khẩu không khớp !',
        ];
    }
}
