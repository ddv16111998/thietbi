<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateBlockPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'school_id' => 'required|regex:/[0-9]{1,}/',
            'count_class' => 'regex:/[0-9]{0,}/',
            'count_std' => 'regex:/[0-9]/',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên khối không được để trống',
            'code.required' => 'Mã khối không được để trống',
            'school_id.required' => 'Trường không được để trống',
            'school_id.regex' => 'Lỗi',
            'count_class.regex' => 'Không đúng định dạng',
            'count_std.regex' => 'Không đúng định dạng',
        ];
    }
}
