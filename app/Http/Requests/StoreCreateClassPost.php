<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreCreateClassPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'name' => 'required',
            'school_id' => 'required',
            'block_id' => 'required',
            'start' => 'required',
            'end' => 'required',
            'gvcn_name' => 'required',
        ];
        if(!empty($request->gvcn_phone)){
            $rules += [ 'gvcn_phone' => 'regex:/^\d{8,11}$/', ];
        }
        if(!empty($request->gvcn_phone)){
            $rules += [ 'gvcn_email' => 'email', ];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên lớp không được để trống',
            'school_id.required' => 'Lựa chọn trường học',
            'block_id.required' => 'Lựa chọn khối học',
            'start.required' => 'Năm học không được để trống',
            'end.required' => 'Năm học không được để trống',
            'gvcn_name.required' => 'Tên giáo viên chủ nghiệm không được để trống',
            'gvcn_email.regex' => 'Email không đúng định dạng',
            'gvcn_phone.regex' => 'Số điện thoại không đúng định dạng',
        ];
    }
}
