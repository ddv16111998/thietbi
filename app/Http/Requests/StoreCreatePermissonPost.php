<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreatePermissonPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required|max:50',
            'route_permisson' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'display_name.required' => 'Tên vai trò không được để trống',
            'route_permisson.required' => 'Đường dẫn không được để trống'
        ];
    }
}
