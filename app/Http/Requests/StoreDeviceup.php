<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'create_date' => 'required',
            'note' => 'required',
            'year_link' => 'required',
            'device_id' => 'required',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Vui lòng điền thông tin',
            'create_date.required' => 'Vui lòng điền thông tin',
            'note.required' => 'Vui lòng điền thông tin',
            'year_link.required' => 'Vui lòng điền thông tin',
            'device_id.required' => 'Vui lòng chọn thiết bị',
        ];
    }
}
