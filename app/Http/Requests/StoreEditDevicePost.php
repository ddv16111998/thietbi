<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreEditDevicePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = (int)$request->route('id');
        return [
            'name'   => 'required',
            'code'   => 'required|unique:devices,code,'.$id,   
            'origin' => 'required',
            'qty'    => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Bắt buộc phải nhập tên thiết bị',
            'code.required' =>'Bắt buộc phải nhập mã thiết bị',
            'code.unique'  => 'Mã thiết bị đã tồn tại.',
            'origin.required' => 'Bắt buộc phải nhập xuất xứ',
            'qty.required' => 'Bắt buộc phải nhập số lượng',
            'status.required' => 'Bắt buộc phải nhập trạng thái thiết bị'
        ];
    }
}
