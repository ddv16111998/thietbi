<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreEditUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = (int)$request->route('id');
        return [
            'name' =>'required',
            'email'=> "required|email|unique:users,email,".$id,
            'acc'   =>'required|alpha_dash|unique:users,acc,'.$id
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Name không được để trống !',
            'email.required'=>'Email không được để trống !',
            'email.email'=>'Email phải đúng định dạng .',
            'email.unique'=>'Email đã tồn tại. Vui lòng chọn email khác !',
            'email.unique'=>'Tài khoản đã tồn tại. Vui lòng chọn tài khoản khác !',
        ];
    }
}
