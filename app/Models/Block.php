<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Block extends Model
{
  use SoftDeletes;

  protected $table = 'blocks';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function school()
  {
      return $this->belongsTo('App\Models\School');
  }

  public  function  subject()
  {
      return $this->hasMany('App\Models\Subject');
  }
  public function classes()
  {
      return $this->hasMany('App\Models\Classes','block_id');
  }
}
