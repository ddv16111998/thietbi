<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Broken extends Model
{
	use SoftDeletes;

  protected $table = 'brokens';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function device()
  {
  	return $this->belongsToMany('App\Models\Device')->withPivot('amount');
  }

  public function brokenDevice()
  {
    return $this->hasMany('App\Models\BrokenDevice');
  }
}
