<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
  use SoftDeletes;

  protected $table = 'category';
  protected $guarded =[];
  protected $dates =['deleted_at'];
  

  public function device()
  {	return $this->hasMany('App\Models\Device');
  	
  }
}
