<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'devvn_tinhthanhpho';
    protected $guarded = [];

    public function district()
    {
    	return $this->hasMany('App\Models\District');
    }
}
