<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;
    protected  $table = 'classroom';
    protected  $guarded = [];
    protected $dates =['deleted_at'];

    public  function school()
    {
        return $this->belongsTo('App\Models\School');
    }
    public  function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }
}
