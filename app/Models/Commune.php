<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
  protected $table = 'devvn_xaphuongthitran';
  protected $guarded = [];

  public function district()
  {
  	return $this->belongsTo('App\Models\District');
  }
}
