<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{

    use SoftDeletes;

    protected $table = 'devices';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    //4 bảng là school,  block,class và classroom
    function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id');
    }

    function block()
    {
        return $this->belongsTo('App\Models\Block', 'block_id');
    }

    function classes()
    {
        return $this->belongsToMany('App\Models\Classes', 'class_devices','device_id')->withPivot('device_id','classes_id');
    }

    function classroom()
    {
        return $this->belongsToMany('App\Models\Classroom', 'classroom_devices','device_id');
    }

    public function BrokenDevice()
    {
        return $this->belongsToMany('App\Models\BrokenDevice');
    }

    public function broken_repair()
    {
        return $this->belongsToMany('App\Models\Broken_repair');
    }

    public function DeviceupDevice()
    {
        return $this->belongsToMany('App\Models\DeviceupDevice');
    }

    public function school_year()
    {
        return $this->belongsTo('App\Models\SchoolYear', 'school_year_id');
    }
//    public function school_devices()
//    {
//        return $this->hasOne('App\Models\');
//    }
}
