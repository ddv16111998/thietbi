<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceBorrow extends Model
{
    use SoftDeletes;

    protected $table = 'devices_borrow';
    protected $guarded=[];
    protected $dates = ['deleted_at'];

    public function device()
    {
    	return $this->belongsToMany('App\Models\Device','borrow_device','borrow_id')->withPivot('device_id');
    }
    public function classes()
    {
        return $this->belongsTo('App\Models\Classes','classes_id');
    }
    public function school_year()
    {
        return $this->belongsTo('App\Models\SchoolYear','school_year_id');
    }
}
