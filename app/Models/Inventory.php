<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Inventory extends Model
{
    use SoftDeletes;
    protected  $table ='inventorys';
    protected $guarded =[];
    protected $dates =['deleted_at'];
    //

    public function device()
    {
        return $this->belongsToMany('App\Models\Device')->withPivot('amount_broken','amount_lost');
    }
}
