<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permisson extends Model
{
  use SoftDeletes;

  protected $table = 'permission';
  protected $guarded =[];
  protected $dates =['deleted_at'];  
}
