<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;
    protected  $table = 'subject';
    protected  $guarded = [];
    protected $dates =['deleted_at'];

    public  function block()
    {
        return $this->belongsTo('App\Models\Block');
    }
}
