<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table='users';
    
    protected $dates =['deleted_at'];

    protected $fillable = [
        'username', 'email', 'password', 'avatar', 'acc','role_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function theme()
    {
        return $this->hasOne('App\Models\UserTheme');
    }
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function department()
    {
        return $this->belongsToMany('App\Models\Department');
    }
    public function position()
    {
        return $this->belongsToMany('App\Models\Position');
    }
    public function diary()
    {
        return $this->hasMany('App\Models\Diary');
    }
}
