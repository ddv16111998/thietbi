<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTheme extends Model
{
    protected $table = 'users_theme';

    protected $fillable = [
        'theme'
    ];
}
