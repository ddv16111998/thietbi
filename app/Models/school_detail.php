<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class school_detail extends Model
{
  protected $table = 'school_details';
	protected $guarded =[];

  public function school()
  {
  	return $this->hasOne('App\Models\School');
  }
}
