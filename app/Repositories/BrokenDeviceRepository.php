<?php

namespace App\Repositories;

use App\Models\BrokenDevice;
use App\Models\Devices;
class BrokenDeviceRepository
{
    public function getData($device_id)
    {
        $data = BrokenDevice::where('device_id',$device_id)->get();
        return $data;
    }

    public function getIndex()
    {
        $data = BrokenDevice::all();
        return $data->toArray();
    }
}
