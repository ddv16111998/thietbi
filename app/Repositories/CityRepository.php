<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\District;
use App\Models\Commune;

class CityRepository
{
    public function getAllCity()
    {
       return City::All();
    }
    public function getAllDistrict()
    {
       return District::All();
    }
    public function getAllCommune()
    {
       return Commune::All();
    }
    public function getDistrictByCity($matp)
    {
        return District::where('matp', $matp)->get();
    }
    public function getCommuneByDistrict($maqh)
    {
        return Commune::where('maqh', $maqh)->get();
    }
}