<?php

namespace App\Repositories;

use App\Models\Classes;
use App\Models\Block;
use App\Models\School;

class ClassesRepository
{
    public function getDataIndex()
    {
       return Classes::whereNull('deleted_at')->get();
    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $id = null) {
        $classes = Classes::updateOrCreate([
            'id' =>$id,
         ],$data);
         if($classes->id)
         {
            return true;
         }
         return false;
    }

    public function getDataEdit($id)
    {
        $classes = Classes::select()->findOrFail($id);
        return $classes->toArray();
    }

    public function destroy($id)
    {
        $delete = Classes::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
    public function getDataById($id)
    {
        $classes = Classes::find($id);
        return $classes;
    }
}
