<?php

namespace App\Repositories;

use App\Models\Classroom;
use App\Models\School;
use App\Models\Subject;

class ClassroomRepository
{
    public function getDataIndex()
    {
        $data = Classroom::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataCreate()
    {
        $data['school'] = School::all(['id','name']);
        $data['block'] = Subject::all(['id','name']);
        return $data;
    }

    public function updateOrCreate($data, $param = null, $id = null)
    {
        $classroom = Classroom::updateOrCreate([
            'id' =>$id,
        ],$data);
        if($classroom->id)
        {
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $device = Classroom::select('id','school_id','subject_id','name','type','classification','acreage','officers','is_functional_classroom')->findOrFail($id);
        return $device->toArray();
    }

    public function destroy($id)
    {
        $delete = Classroom::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
