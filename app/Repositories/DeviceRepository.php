<?php

namespace App\Repositories;

use App\Models\Device;
use App\Models\Classroom;
use App\Models\Block;
use App\Models\Subject;
use App\Models\DeviceBorrow;
use App\Models\Device_Repair;
use App\Models\Classes;
use App\Models\BrokenDevice;
use App\Models\Broken_repair;

class DeviceRepository
{
    // device
    public function getDataIndex()
    {
        $data = Device::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataSubject(){
        $data['subject'] = Subject::all(['id','name']);
        return $data;
    }
    public function getDataDeviceById($id)
    {
        return Device::find($id);
    }
    public function getDataCreate()
    {
        $classrooms = Classroom::all(['id','name']);
        return $classrooms;
    }
    public function getDataClasses()
    {
        $data = Classes::all(['id','name']);
        return $data;
    }
    public function updateOrCreate($data, $rel_data = null, $id = null) {
         $device = Device::updateOrCreate([
            'id' =>$id,
         ],$data);
        if(!empty($id)){
            $device->classes()->detach();
            $device->classroom()->detach();
        }
         if($device->id)
         {
             foreach ($rel_data['classes_id'] as $val)
             {
                 $device->classes()->attach($device->id,['classes_id'=>$val]);
             }
            foreach ($rel_data['classroom_id'] as $value)
            {
                $device->classroom()->attach($device->id,['classroom_id'=>$value]);
            }

             return true;
         }
         return false;
    }

    public function getDataEdit($id)
    {
        $device = Device::find($id);
        return $device;
    }

    public function destroy($id)
    {
        $delete = Device::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }

    // device Borrow
    public function getDecivesBorrow()
    {
        $data = DeviceBorrow::whereNull('deleted_at')->get();
        return $data;
    }
    public function updateOrCreateBorrow($data,$param,$id = null)
    {
        $deviceBorrow = DeviceBorrow::updateOrCreate([
            'id' => $id,
        ],$data);
        if(!empty($id))
        {
            $deviceBorrow->device()->detach();
        }
        if($deviceBorrow->id)
        {
            foreach ($param['device_id'] as $key =>$value)
            {
                $deviceBorrow->device()->attach($deviceBorrow->id,[
                   'device_id'=> $value,
                    'amount'  =>$param['amount'][$key]
                ]);
            }
            return true;
        }
        return false;
    }
    public function getDataBorrowById($id)
    {
        $data = DeviceBorrow::find($id);
        return $data;
    }
    public function destroyBorrow($id)
    {
        $delete = DeviceBorrow::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }



    //repository Repair
    public function updateOrCreateRepair($data,$rel_data = null,$id = null)
    {
        $deviceRepair = Device_Repair::updateOrCreate(['id' => $id],$data);
        if(!empty($id)){
            $deviceRepair->device()->detach();
        }
        if($deviceRepair->id)
        {
            foreach ($rel_data['device_id'] as $k => $v)
            {
                $deviceRepair->device()->attach($deviceRepair->id,[
                    'device_id' => $v,
                    'amount' => $rel_data['amount'][$k],
                    'price'  => $rel_data['price'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function  getDataRepairIndex()
    {
        $data = Device_Repair::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataRepairById($id)
    {
        $data =Device_Repair::find($id);
        return $data;
    }

    public function destroyRepair($id)
    {
        $delete = Device_Repair::find($id);
        $delete->delete();
        $delete->device()->detach();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }

    // repository Broken_repair
    public function Broken_Repair_By_RepairId($repair_id)
    {
        $data = Broken_repair::where('repair_id',$repair_id)->get();
        return $data;
    }
    public function  Broken_Repair_By_Id($id)
    {
        $data = Broken_repair::find($id);
        return $data;
    }
    public function AllDataBroken_Repair()
    {
        $data = Broken_repair::all();
        return $data;
    }



}
