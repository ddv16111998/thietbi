<?php

namespace App\Repositories;

use App\Models\Deviceup;
use App\Models\Device;

class DeviceupRepository
{
    public function getDataIndex()
    {
       return Deviceup::whereNull('deleted_at')->get();
    }

    public function getDataCreate()
    {
       
    }

    public function getById($id)
    {
        return Deviceup::find($id);
    }

    public function getDataSearch($param)
    {
        return Device::whereNull('deleted_at')->where($param)->get();
    }

    public function updateOrCreate($data, $rel_data = null, $id = null) {
        $total = 0;
        foreach ($rel_data['device_id'] as $vt => $it) {
            $dg = Device::find($it);
            $total += $rel_data['amount'][$vt]*$dg->price;
        }

        $data['total_money'] = $total;
        $deviceup = Deviceup::updateOrCreate(['id' =>$id,],$data);
        if(!empty($id)){
            $devices = $deviceup->device()->get();
            foreach ($devices as $device) {
                $amount = Device::find($device->id);
                $device_amount = [
                    'amount' => $amount->amount-$device->pivot->amount,
                ];
                Device::updateOrCreate(['id'=>$device->id], $device_amount);
            }
            $deviceup->Device()->detach();
        }

        if($deviceup->id){
            foreach ($rel_data['device_id'] as $k => $v) {
                $amount = Device::find($v);
                $device_amount = [
                    'amount' => $amount->amount+$rel_data['amount'][$k],
                ];
                Device::updateOrCreate(['id'=>$v], $device_amount);
                $deviceup->Device()->attach($deviceup->id, [
                    'device_id' => $v,
                    'amount' => $rel_data['amount'][$k],
                    'note' => $rel_data['note'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $deviceup = Deviceup::select()->findOrFail($id);
        return $deviceup->toArray();
    }

    public function destroy($id)
    {
        $delete = Deviceup::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}