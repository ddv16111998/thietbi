<?php

namespace App\Repositories;

use App\Models\Inventory;

class InventoryRepository
{
    public function getDataIndex()
    {
        $data = Inventory::whereNull('deleted_at')->get();
        return $data;

    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $param = null, $id = null) {
        $inventory = Inventory::updateOrCreate(['id'=>$id],$data);
        if(!empty($id))
        {
            $inventory->device()->detach();
        }
        if($inventory->id)
        {
            foreach ($param['device_id'] as $k => $v)
            {
                $inventory->device()->attach($inventory->id,[
                    'device_id' => $v,
                    'amount_broken' => $param['amount_broken'][$k],
                    'amount_lost'   => $param['amount_lost'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $data = Inventory::find($id);
        return $data;
    }

    public function destroy($id)
    {
        $delete = Inventory::find($id);
        $delete->device()->detach();
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
