<?php

namespace App\Repositories;

use App\Models\Permisson;

class PermissonRepository
{
    public function getDataIndex()
    {
       return Permisson::whereNull('deleted_at')->get();
    }

    public function getById($id)
    {
       return Permisson::find($id);
    }

    public function setPermisson($data)
    {
        return Permisson::insert($data);
    }

    public function getDataCreate()
    {
       
    }

    public function updateOrCreate($data, $param = null, $id = null) {
        
    }

    public function getDataEdit()
    {

    }

    public function setUpdate($id, $data)
    {
        $Permisson = Permisson::find($id);
        foreach ($data as $key => $value) {
          $Permisson->$key = $value;
        }
        return $Permisson->save();
    }

    public function destroy($id)
    {
        $delete = Permisson::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}