<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository
{
    public function getDataIndex()
    {
       return Role::whereNull('deleted_at')->get();
    }

    public function getDataCreate()
    {
        $route_name = array();
        foreach (\Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            if(isset($action['as'])) {
                if (array_key_exists('as', $action) AND preg_match("/^(admin.)[a-z]/", $action['as']) AND !preg_match("/(.store)$/", $action['as']) AND in_array('GET', $route->methods) AND !in_array('POST', $route->methods)) {
                    if (preg_match("/(.index)$/", $action['as']) OR preg_match("/(.create)$/", $action['as']) OR preg_match("/(.remove)$/", $action['as']) OR preg_match("/(.destroy)$/", $action['as']) OR preg_match("/(.edit)$/", $action['as'])) {
                        $inarr = explode('.', trim(str_replace('admin.', ' ', $action['as'])));
                        krsort($inarr);
                        $display_name = implode(' ', $inarr);
                        $route_name[] = [
                            'route'         => $action['as'],
                            'display_name'  => ucwords(strtolower(str_replace('index','list',$display_name)))
                        ];
                    }
                }
            }
        }
        return $route_name;
    }

    public function updateOrCreate($data, $id = null) {
        $role = Role::updateOrCreate([
            'id' => $id,
        ], $data);
        if($role->id) {
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $route_name = array();
        foreach (\Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            if(isset($action['as'])) {
                if (array_key_exists('as', $action) AND preg_match("/^(admin.)[a-z]/", $action['as']) AND !preg_match("/(.store)$/", $action['as']) AND in_array('GET', $route->methods) AND !in_array('POST', $route->methods)) {
                    if (preg_match("/(.index)$/", $action['as']) OR preg_match("/(.create)$/", $action['as']) OR preg_match("/(.remove)$/", $action['as']) OR preg_match("/(.destroy)$/", $action['as']) OR preg_match("/(.edit)$/", $action['as']) OR preg_match("/(.excel)$/", $action['as']) OR preg_match("/(.follow)$/", $action['as']) OR preg_match("/(.update-status)$/", $action['as']) OR preg_match("/(.track_book)$/", $action['as']) OR preg_match("/(.excel)$/", $action['as']) OR preg_match("/(.create_fast)$/", $action['as']) OR preg_match("/(.create_broken_lost)$/", $action['as'])) {
                        $inarr = explode('.', trim(str_replace('admin.', ' ', $action['as'])));
                        krsort($inarr);
                        $display_name = implode(' ', $inarr);
                        $route_name[] = [
                            'route'         => $action['as'],
                            'display_name'  => ucwords(strtolower(str_replace('index','list',$display_name)))
                        ];
                    }
                }
            }
        }
        $data['route_name'] = $route_name;
        $data['role'] = $role = Role::find($id);
        $data['permissions'] = explode('|', $role->permissions);
        return $data;
    }

    public function destroy($id)
    {
        $delete = Role::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
