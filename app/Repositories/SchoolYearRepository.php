<?php

namespace App\Repositories;

use App\Models\SchoolYear;

class SchoolYearRepository
{
    public function getDataIndex()
    {
        $data = SchoolYear::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $id) {
        $school_year = SchoolYear::updateOrCreate(['id'=>$id],$data);
        if($school_year)
        {
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $data = SchoolYear::find($id);
        return $data;
    }

    public function destroy($id)
    {
        $delete = SchoolYear::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
