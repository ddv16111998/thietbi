<?php

namespace App\Repositories;

use App\Models\Subject;
use App\Models\Block;

class SubjectRepository
{
    public function getDataIndex()
    {
        $data = Subject::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataCreate()
    {
        $data['block'] = Block::all(['id','name']);
        return $data;
    }

    public function updateOrCreate($data, $id = null)
    {
        $subject = Subject::updateOrCreate([
            'id' =>$id,
        ],$data);
        if($subject->id)
        {
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $device = Subject::select('id','name','block_id','school_year')->findOrFail($id);
        return $device->toArray();
    }

    public function destroy($id)
    {
        $delete = Subject::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
