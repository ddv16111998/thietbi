<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use App\Models\Position;
use App\Models\Department;

class UserRepository
{

    public function getDataUsers()
    {
        $data = User::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataRole()
    {
        return Role::select('id','name')->whereNull('deleted_at')->get();
    }

    public function updateOrCreate($data, $param, $id = null) {
        $user = User::updateOrCreate([
            'id' => $id,
        ], $data);
        if($user->id) {
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $user       = User::select('id','username as name','email','avatar','status', 'acc')->findOrFail($id);
        $data['user']       = $user->toArray();
        return $data;
    }

    public function destroy($id)
    {
        $delete = User::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}