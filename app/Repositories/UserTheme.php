<?php

namespace App\Repositories;

use App\Models\UserTheme;
use Illuminate\Support\Facades\DB;

class UserThemeRepository
{
    public static function changeTheme($theme)
    {
        return UserTheme::where('user_id', \Auth::user()->id)->update(['theme' => $theme]);
    }
}