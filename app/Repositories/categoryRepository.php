<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    public function getDataIndex()
    {
       return Category::whereNull('deleted_at')->get();
    }

    public function getDataCreate()
    {
       
    }

    public function updateOrCreate($data, $param = null, $id = null) {
        
    }

    public function getDataEdit($id)
    {

    }

    public function destroy($id)
    {
        $delete = Category::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }



  /**
   * Lấy danh mục theo id danh mục
   * @param  $id - id của danh mục
   * @return mảng dữ liệu của danh mục
   */
  public function getById($id)
  {
    return Category::find($id);
  }

  /**
   * thêm dữ liệu vào bảng 
   * @param mảng dữ liệu dạng ['column' => 'value',]
   */
  public function setCate($data)
  {
    return Category::insert($data);
  }

  public function setUpdate($id, $data)
  {
    $cate = Category::find($id);
    foreach ($data as $key => $value) {
      $cate->$key = $value;
    }
    return $cate->save();
  }

  public function getParentId($parent_id)
  {
    return Category::where('parent_id', $parent_id)->get();
  }
  
}