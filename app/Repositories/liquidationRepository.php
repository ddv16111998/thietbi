<?php

namespace App\Repositories;

use App\Models\Liquidation;
use App\Models\Device;

class LiquidationRepository
{
    public function getDataIndex()
    {
        $data = Liquidation::whereNull('deleted_at')->get();
        return $data;
    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $param = null, $id = null) {
        $liquidation = Liquidation::updateOrCreate(['id'=>$id],$data);
        if(!empty($id))
        {
            $liquidation->device()->detach();
        }
        if($liquidation->id)
        {
            foreach ($param['device_id'] as $k =>$v)
            {
                $liquidation->device()->attach($liquidation->id,[
                    'device_id' => $v,
                    'amount'    => $param['amount'][$k],
                    'note'      => $param['note'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        return Liquidation::find($id);
    }

    public function destroy($id)
    {
        $data = Liquidation::find($id);
        $data->device()->detach(); // xóa bảng trung gian chứa khóa ngoại trước
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return fasle;
    }
    public function getDataSearch($param)
    {
        return Device::whereNull('deleted_at')->where($param)->get();
    }
}
