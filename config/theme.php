<?php

return [
 	'dark' => '
 				.skin-default-dark .left-sidebar, .skin-default-dark .topbar .top-navbar .navbar-header {
				    background: #272425!important;
				}
			',
	'light' => '
			.skin-default-dark .left-sidebar, .skin-default-dark .topbar .top-navbar .navbar-header {
				    background: #fff!important;
				}
			',
	'change' => [
		'dark'  => 'light',
		'light' => 'dark'
	],
	'show' => [
		'dark'  => 'Light',
		'light' => 'Dark'
	]
];