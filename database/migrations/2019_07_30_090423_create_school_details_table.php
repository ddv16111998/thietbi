<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('school_id')->nullable();
            $table->string('image_maps')->nullable();
            $table->string('image_schedule')->nullable();
            $table->string('year_link')->nullable();
            $table->string('total_class')->nullable();
            $table->string('level')->nullable();
            $table->date('principal_birthday')->nullable();
            $table->string('principal_phone')->nullable();
            $table->string('principal_email')->nullable();
            $table->string('vice_principal_name')->nullable();
            $table->date('vice_principal_birthday')->nullable();
            $table->string('vice_principal_phone')->nullable();
            $table->string('vice_principal_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_details');
    }
}
