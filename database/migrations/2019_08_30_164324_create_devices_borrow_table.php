<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesBorrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices_borrow', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('time_borrow');
            $table->date('time_return');
            $table->string('teacher');
            $table->string('discipline');
            $table->unsignedBigInteger('school_year_id');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->string('mass')->nullable();
            $table->unsignedBigInteger('school_id');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->unsignedBigInteger('block_id');
            $table->foreign('block_id')->references('id')->on('blocks');
            $table->unsignedBigInteger('classes_id');
            $table->foreign('classes_id')->references('id')->on('classes');
            $table->text('note')->nullable();
            $table->string('lesson_name');
            $table->string('lesson_num');
            $table->integer('lesson_use');
            $table->integer('is_practice');
            $table->date('return_date')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices_borrow');
    }
}

