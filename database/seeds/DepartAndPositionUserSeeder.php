<?php

use Illuminate\Database\Seeder;

class DepartAndPositionUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$department = DB::table('departments')->insertGetId(
            [
                'name' 	=> 'Kinh Doanh',
            ]
        );

    	$position = DB::table('positions')->insertGetId(
            [
                'name' 	=> 'Giám Đốc',
            ]
        );
        $users = DB::table('users')->select('id')->get();
    	foreach ($users as $key => $value) {
    		DB::table('department_user')->insert(
	            [
                    'user_id'   => $value->id,
                    'department_id' => $department
                ]
	        );
	        DB::table('position_user')->insert(
	            [
                    'user_id'   => $value->id,
                    'position_id' => $position
                ]
	        );
    	}
    }
}
