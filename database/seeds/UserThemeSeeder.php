<?php

use Illuminate\Database\Seeder;

class UserThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = DB::table('users')->select('id')->get();
    	foreach ($users as $key => $value) {
    		DB::table('users_theme')->insert(
	            [
	                [
	                    'user_id' 	=> $value->id,
	                    'theme'		=> 'light'
	                ],
	            ]
	        );
    	}
    	
    }
}
