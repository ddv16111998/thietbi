<?php 
return array (
  'name' => '公司名',
  'affiliated_company' => '联营公司',
  'description' => '描述',
  'add_company' => '添加新公司',
  'select_company' => '选择公司',
  'nested_company' => '多层次结构',
  'warning_save' => '警告！ 您有未保存的更改。',
  'select_language' => '选择语言',
  'post_code' => '公司代码',
  'prefix_account' => '帐户前缀',
  'noted' => '注意',
  'token' => '代币',
  'change_token' => '改变令牌',
  'company_type' => '改变令牌',
);
