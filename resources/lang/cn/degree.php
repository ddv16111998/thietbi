<?php 
return array (
  'degree' => '教育水平',
  'title_degree_index' => '管理教育',
  'title_degree_updated' => '教育更新',
  'degree_name' => '教育名称',
  'degree_description' => '教育说明',
  'title_degree_created' => '添加教育',
  'guide_input_name' => '请输入教育名称',
  'guide_input_description' => '请输入教育说明',
  'validate_min' => '名称必须至少包含三个字符',
  'validate_max' => '名称最多只能包含255个字符',
  'name_unique' => '学术名称已存在，请输入其他名称',
);
