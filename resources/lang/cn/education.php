<?php 
return array (
  'education' => '教育的地方',
  'title_education_index' => '教育管理',
  'title_education_updated' => '教育更新',
  'education_name' => '教育的地方',
  'education_description' => '描述',
  'title_education_created' => '添加教育',
  'guide_input_name' => '请输入教育名称',
  'guide_input_description' => '请描述培训地点',
  'validate_min' => '名称必须至少包含三个字符',
  'validate_max' => '名称最多只能包含255个字符',
);
