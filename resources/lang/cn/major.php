<?php 
return array (
  'major' => '主要',
  'title_major_index' => '主要管理',
  'title_major_updated' => '重大更新',
  'major_name' => '主要名称',
  'major_description' => '主要描述',
  'title_major_created' => '添加专业',
  'guide_input_name' => '请输入主要名称',
  'guide_input_description' => '请输入专业的说明',
  'validate_min' => '名称必须至少包含三个字符',
  'validate_max' => '名称最多只能包含255个字符',
  'name_unique' => '专业名称已存在，请输入其他名称',
);
