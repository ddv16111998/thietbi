<?php 
return array (
  'status' => '状态',
  'name' => '状态名称',
  'no_revenue' => '没有收入',
  'no_reach' => '无法访问',
  'weight' => '顺序',
  'color' => '颜色',
  'level' => '水平',
  'system_status' => '系统状态',
  'no_reach_explain' => '状态未计入到达客户',
  'no_revenue_explain' => '员工未确认的收入状况',
  'guide_level' => '将状态收集到一个组中',
  'guide_weight' => '排序状态',
);
