<?php 
return array (
  'required_custom' => '这是必填栏。',
  'required' => '不能留空',
  'regex' => '格式无效',
  'min' => '必须至少',
  'max' => '必须更大',
  'unique' => '已经存在',
  'numeric' => '该属性必须是数字',
  'string' => '该属性必须是字符串',
  'password_not_different' => '择式',
  'not_is_create_data' => '择式',
);
