<?php 
return array (
  'name' => 'Company Name',
  'affiliated_company' => 'Affiliated Company',
  'description' => 'Description',
  'add_company' => 'Add New Company',
  'select_company' => 'Select Company',
  'nested_company' => 'Multi-level Structure',
  'warning_save' => 'Warning! You have unsaved changes.',
  'select_language' => 'Select Language',
  'post_code' => 'Company Code',
  'prefix_account' => 'Account Prefix',
  'noted' => 'Note',
  'token' => 'Token',
  'change_token' => 'Change Token',
  'company_type' => 'Company Type',
);
