<?php 
return array (
  'degree' => 'Education level',
  'title_degree_index' => 'Degree manage',
  'title_degree_updated' => 'Degree update',
  'degree_name' => 'Degree name',
  'degree_description' => 'Degree description',
  'title_degree_created' => 'Add Degree',
  'guide_input_name' => 'Please enter the Degree name',
  'guide_input_description' => 'Please enter a description for the Education',
  'validate_min' => 'The name must contain at least 3 characters',
  'validate_max' => 'The name can only be up to 255 characters',
  'name_unique' => 'The academic name already exists, please enter another name',
);
