<?php 
return array (
  'education' => 'Education place',
  'title_education_index' => 'Education manage',
  'title_education_updated' => 'Education update',
  'education_name' => 'Education place',
  'education_description' => 'Description',
  'title_education_created' => 'Add Education',
  'guide_input_name' => 'Please enter the Education name',
  'guide_input_description' => 'Please describe the place of Education',
  'validate_min' => 'The name must contain at least 3 characters',
  'validate_max' => 'The name can only be up to 255 characters',
);
