<?php 
return array (
  'name' => 'Order',
  'customer_name' => 'Customer Name',
  'customer_phone' => 'Phone',
  'customer_address' => 'Customer Address',
  'province' => 'Province/City',
  'customer_city_id' => 'Province/City',
  'district' => 'District',
  'customer_district_id' => 'District',
  'ward' => 'Ward',
  'customer_ward_id' => 'Ward',
  'order_code' => 'Order Code',
  'id' => 'Order Code',
  'auto_generate' => 'Auto Generate',
  'postal_code' => 'Postal Code',
  'status' => 'Status',
  'status_id' => 'Status',
  'change_status' => 'Change Status',
  'bundle_id' => 'Classify',
  'source_id' => 'Source',
  'order_type' => 'Order Type',
  'user_created' => 'User Created',
  'user_confirmed' => 'User Confirmed',
  'user_assigned' => 'Assigned User',
  'user_upsale' => 'UpSale',
  'fb_post_id' => 'ID Post',
  'fb_page_id' => 'ID FanPage',
  'fb_customer_id' => 'Id FB Customer',
  'search_product' => 'Search Product',
  'product_name' => 'Product Name',
  'unit' => 'Unit',
  'quantity' => 'Quantity',
  'unit_price' => 'Unit Price',
  'discount' => 'Discount',
  'amount' => 'Amount',
  'shipping_price' => 'Shipping Price',
  'surcharge' => 'Surchange',
  'total_price' => 'Total Price',
  'view_history' => 'View History',
  'product_goods' => 'Products',
  'pay_info' => 'Billing Information',
  'fb_info' => 'Facebook Information',
  'order_info' => 'Order Information',
  'customer_info' => 'Customer Information',
  'note_deliver' => 'Deliver Note',
  'add_customer' => 'Add Customer',
  'no_data' => 'Please select the product.',
  'created_at' => 'Created Date',
  'stt' => 'STT',
  'confirmed_at' => 'Confirmed Date',
  'filter_date' => 'Filter Date',
  'from_day' => 'From Day',
  'to_day' => 'To Day',
  'check_all' => 'Select All',
  'deselect_all' => 'Deselect All',
  'search_advanced' => 'Search Advanced',
  'enter_list' => 'Please :link to view order list.',
  'export_excel' => 'Export Excel',
  'import_excel' => 'Import Excel',
  'config_show_column' => 'Adjust Column Display',
  'detail' => 'Order Detail',
  'shipping_id' => 'Transport',
  'company_name' => 'Company',
);
