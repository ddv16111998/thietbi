<?php 
return array (
  'position' => 'Position',
  'title_position_index' => 'Position manage',
  'title_position_updated' => 'Position update',
  'position_name' => 'Position name',
  'payroll_rate' => 'Position name',
  'required_payroll_rate' => 'Please enter a salary level',
  'validate_payroll_rate' => 'Wrong format. For example: 1.0 , 0.5 , 2',
  'required_payroll_default' => 'Please enter basic salary',
  'payroll_default' => 'Position name',
  'title_position_created' => 'Add Position',
  'guide_input_name' => 'Please enter the position name',
  'validate_min' => 'The name must contain at least 3 characters',
  'validate_max' => 'The name can only be up to 50 characters',
  'name_unique' => 'The name of the job already exists, enter another name',
  'choose_position' => 'Choose position',
  'title_full_name' => 'Full name',
  'position_job' => 'Job position',
  'work_unit' => 'Work unit',
  'empty_info' => 'No employee information yet',
);
