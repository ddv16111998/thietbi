<?php 
return array (
  'status' => 'Status',
  'name' => 'Status Name',
  'no_revenue' => 'No revenue',
  'no_reach' => 'No reach',
  'weight' => 'Weight',
  'color' => 'Color',
  'level' => 'Level',
  'system_status' => 'System Status',
  'no_reach_explain' => 'Status not counted as reaching the customer.',
  'no_revenue_explain' => 'Unconfirmed revenue status for employees.',
  'guide_level' => 'Collect status into a group.',
  'guide_weight' => 'Sort Status.',
);
