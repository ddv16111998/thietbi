<?php 
return array (
  'required_custom' => 'This field is required.',
  'required' => 'field is required.',
  'regex' => 'format is invalid',
  'min' => 'must be at least',
  'max' => 'must be at greater',
  'unique' => 'already exists',
  'numeric' => 'The attribute must be a number',
  'string' => 'The attribute must be a string',
  'password_not_different' => 'Password must be different from current password',
  'not_is_create_data' => 'Permission denied',
);
