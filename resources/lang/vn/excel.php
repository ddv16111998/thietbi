<?php 
return array (
  'name' => 'Nhập file excel',
  'name_1' => 'Import đơn hàng từ excel (Marketing)',
  'name_2' => 'Thực hiện import',
  'name_3' => 'Tải excel lên',
  'name_4' => 'Tải excel mẫu',
  'name_5' => 'Tải file excel đã được chỉnh thứ tự cột theo file mẫu (rất quan trọng)',
  'name_6' => 'Nhấn Thực hiện import để hoàn thành.',
  'step_1' => 'Bước 1',
  'step_2' => 'Bước 2',
);
