<?php 
return array (
  'status' => 'Trạng thái',
  'name' => 'Tên trạng thái',
  'no_revenue' => 'Không doanh thu',
  'no_reach' => 'Không tiếp cận',
  'weight' => 'Vị trí',
  'color' => 'Màu sắc',
  'level' => 'Cấp độ',
  'system_status' => 'Trạng thái hệ thống',
  'no_reach_explain' => 'Là trạng thái không được tính là đã tiếp cận đến khách hàng.',
  'no_revenue_explain' => 'Là trạng thái không được xác nhận doanh thu đối với nhân viên.',
  'guide_level' => 'Để gom trạng thái vào 1 nhóm.',
  'guide_weight' => 'Sắp xếp trạng thái.',
);
