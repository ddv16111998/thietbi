@extends('auth.app')
@section('title') @lang('system.title_login') @endsection
@section('content')
    <form class="form-horizontal form-control-line text-center" id="loginform" action="{{ route('login') }}" method="POST">
        @csrf
        <div class="mt-3 mb-3">
            <h3>@lang('system.login')</h3>
        </div>
        <div class="@haserror('account') form-group @endhaserror">
            @error('account')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group @haserror('username') has-danger @endhaserror">
            <div class="col-xs-12">
                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="@lang('system.username')">
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group @haserror('password') has-danger @endhaserror">
            <div class="col-xs-12">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="@lang('system.password')">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="d-flex no-block align-items-center">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember" value="1">
                        <label class="custom-control-label" for="customCheck1">@lang('system.remember_me')</label>
                    </div> 
                    <div class="ml-auto">
                        @if (Route::has('password.request'))
                            <a id="to-recover" class="text-muted" href="{{ route('password.request') }}">
                                <i class="fas fa-lock m-r-5"></i> @lang('system.forgot_password')?
                            </a>
                        @endif
                    </div>
                </div>   
            </div>
        </div>
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12 d-flex justify-content-center">
                <button class="btn btn-info btn text-uppercase btn-rounded" type="submit">@lang('system.login')</button>
            </div>
        </div>
    </form>
@endsection
