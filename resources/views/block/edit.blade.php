@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') Cập nhật khối @endsection
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Cập nhật khối</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.block.index') }}">@lang('system.manage') khối</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.block.edit') }}">Cập nhật khối</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.block.update') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                
                                @csrf
                                <input type="hidden" name="id" value="{{ $block->id }}">
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Tên khối <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="name" value="{{ $block->name }}" id="name" type="text" class="form-control">
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="code" class="col-md-2 text-center col-form-label">Mã khối<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="code" value="{{ $block->code }}" id="code" type="text" class="form-control" >
                                        @error('code')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="school_id" class="col-md-2 text-center col-form-label">Trường học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="school_id" id="school_id" class="form-control">
                                            <option value="">---None---</option>
                                        @foreach ($schools as $school)
                                            <option value="{{ $school->id }}" {{ ($school->id==$block->school_id) ? 'selected' : '' }}>{{ $school->name }}</option>
                                        @endforeach
                                        </select>
                                        @error('school_id')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <section class="text-right">
                                    <div class="row justify-content-around">
                                        <div class="col-12">
                                        <a href="{{ route('admin.block.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
