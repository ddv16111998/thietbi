@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')khối @endsection
@section('css')
@endsection
@section('js')
    <script>
    $(function() {
        $('#listBlock').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.block.index') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name'},
                { data: 'code', name: 'code'},
                { data: 'school_id', name: 'school_id'},
                { data: 'count_class', name: 'count_class'},
                { data: 'count_std', name: 'count_std'},
                { data: 'action', name: 'action'}
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        }); 
    });
    </script>
@endsection
@section('content')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">
                    Quản lý khối
                </h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.block.index') }}">QL khối</a></li>
                </ol>
                <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.block.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listBlock"class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <th>Tên khối</th>
                                        <th>Mã khối</th>
                                        <th>Tên trường</th>
                                        <th>Tổng số lớp</th>
                                        <th>Tổng số học sinh</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                </table>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
