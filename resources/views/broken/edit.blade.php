@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') Cập nhật Phiếu hỏng/mất @endsection
@section('css')
@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>

    <script>
        $('#search_name').change(function(event) {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            }
        });

        $.ajax({
            url: "{{ route('admin.liquidation.create') }}",
            data: {device_name: $('#search_name').val()},
        })
            .done(function(data) {
            $('#modal_table').html(data);
            $('button.select_device').click(function() {
                var x = $(this).val();
                var data = '<div class="form-group row"><input type="hidden" name="device_id[]" value="'+x+'" /><div class="col-3 text-right">';
                data += $(this).attr('data-device');
                data += '</div><div class="col-3"><input name="amount[]" type="number" min="0" class="form-control" placeholder="0" /></div><div class="col-3"><select class="device-ajax form-control" name="status_device[]"><option value="0">Sửa chữa</option><option value="1">Thanh lý</option><option value="2">Mất</option></select></div><input type="button" value="X" class="btn text-danger" onclick="rm(this)"></div>';
                $('#btnAdd').before(data);
            });
        });
        });
        $('button.select_device').click(function() {
                var x = $(this).val();
                var data = '<div class="form-group row"><input type="hidden" name="device_id[]" value="'+x+'" /><div class="col-3 text-right">';
                    data += $(this).attr('data-device');
                    data += '</div><div class="col-3"><input name="amount[]" type="number" min="0" class="form-control" placeholder="0" /></div><div class="col-3"><select class="device-ajax form-control" name="status_device[]"><option value="0">Sửa chữa</option><option value="1">Thanh lý</option><option value="2">Mất</option></select></div><input type="button" value="X" class="btn text-danger" onclick="rm(this)"></div>';
                $('#btnAdd').before(data);
        });

        function rm(obj) {
            console.log('dfj');
            /* Act on the event */
            var x = $(obj).parent();
            x.remove();
        }
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Cập nhật Phiếu hỏng/mất</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.broken.index') }}">@lang('system.manage') Phiếu hỏng/mất</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.broken.create') }}">Cập nhật phiếu hỏng/mất</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.broken.update', ['id'=>$broken->id]) }}" enctype="multipart/form-data">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="justify-content-md-center">
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Tên phiếu <span class="text-danger">*</span></label>
                                                <input name="name" value="{{ $broken->name }}" id="name" type="text" class="form-control">
                                                @error('name')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="code">Mã phiếu <span class="text-danger">*</span></label>
                                                <input name="code" value="{{ $broken->code }}" id="code" type="text" class="form-control">
                                                @error('code')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Tình trạng <span class="text-danger">*</span></label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="0">Hỏng</option>
                                                    <option value="1">Mất</option>
                                                </select>
                                                @error('status')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @php
                                                    $crdate = explode('-',$broken->create_date);
                                                @endphp
                                                <label for="create_date">Ngày tạo</label>
                                                <input type="text" class="form-control datepicker" id="create_date" name="create_date" value="{{ $crdate[2].'/'.$crdate[1].'/'.$crdate[0] }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Ghi chú</label>
                                                <textarea name="note" class="form-control">{{ $broken->note }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group border">
                                            <label class="text-muted">Chọn thiết bị: <span class="text-danger">(*)</span></label>
                                            @error('device_id')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror

                                            @foreach ($devices as $device)
                                            <div class="form-group row"><input type="hidden" name="device_id[]" value="{{ $device->id }}" /><div class="col-3 text-right">{{ $device->name }}
</div><div class="col-3"><input name="amount[]" type="number" min="0" class="form-control" placeholder="0" value="{{ $device->pivot->amount }}" /></div><div class="col-3"><select name="status_device[]" class="form-control"><option value="0" {{ $device->pivot->stauts==0 ? 'selected' : '' }}>Sửa chữa</option><option value="1" {{ $device->pivot->stauts==1 ? 'selected' : '' }}>Thanh lý</option><option value="2" {{ $device->pivot->stauts==2 ? 'selected' : '' }}>Mất</option></select></div><input type="button" value="X" class="btn text-danger" onclick="rm(this)"></div>
                                                @endforeach

                                            <input type="button" value="+ Thêm thiết bị" class="btn form-control" id="btnAdd" data-toggle="modal" data-target="#myModal">
                                        </div>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.broken.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4>Chọn thiết bị</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
            <div class="col-6">
                <div class="form-group">
                    <label for="">Tên thiết bị</label>
                    <input type="text" class="form-control" placeholder="tìm kiếm tên..." id="search_name">
                </div>
                <div class="form-group">
                    <label>Chọn kho thiết bị</label>
                    <select name="" id="cate_id" class="form-control">
                        <option value="">--None--</option>
                    </select>
                </div>
            </div>
        </div>
        <table class="table">
            <tr>
                <th>Số hiệu</th>
                <th>Mã thiết bị</th>
                <th>Tên thiết bị</th>
                <th>Đơn vị</th>
                <th width="12px"></th>
            </tr>
            <tbody id="modal_table">
            @foreach ($devices_sl as $device)
            <tr>
                <td>{{ $device->so_hieu }}</td>
                <td>{{ $device->code }}</td>
                <td>{{ $device->name }}</td>
                <td>{{ $device->unit }}</td>
                <td>
                    <button class="btn btn-info select_device" value="{{ $device->id }}" data-device="{{ $device->name }}" title="chọn">>>></button>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <input type="button" value="Xong" class="btn btn-info" data-dismiss="modal">
      </div>
    </div>

  </div>
</div>
@endsection
