@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Phiếu hỏng/mất @endsection
@section('css')
@endsection
@section('js')
    <script>
        $('#listBroken').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.broken.index') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name'},
                { data: 'code', name: 'code'},
                { data: 'status', name: 'status'},
                { data: 'device', name: 'device'},
                { data: 'create_date', name: 'create_date'},
                { data: 'note', name: 'note'},
                { data: 'action', name: 'action'}
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    </script>
@endsection
@section('content')
    @include('layouts.nav-tabs')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">
                    Quản lý Phiếu hỏng/mất
                </h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.broken.index') }}">Phiếu hỏng/mất</a></li>
                </ol>
                <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.broken.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row text-right"><a  href="{{ route('admin.broken.excel') }}" target=_self accesskey="x" class="btn btn-outline-success">Xuất Excel</a></div>
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listBroken"class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 12px;">ID</th>
                                        <th>Tên phiếu</th>
                                        <th>Mã phiếu</th>
                                        <th>Trạng thái</th>
                                        <th>Thiết bị</th>
                                        <th>Ngày tạo</th>
                                        <th>Ghi chú</th>
                                        <th style="width: 120px;">@lang('system.operations')</th>
                                    </tr>
                                    </thead>
                                </table>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
