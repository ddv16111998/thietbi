<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>
      CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO
    </title>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" /> -->
    <link rel="shortcut icon" href="images/logo.ico" />

    <link href="/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    

    <link href="/assets/css/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/morris.css" rel="stylesheet">
    <link href="/assets/css/jquery.toast.css" rel="stylesheet">
    <link href="/assets/css/style.min.css" rel="stylesheet">
    <link href="/assets/css/dashboard1.css" rel="stylesheet">
    <link href="/assets/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/fontawesome/css/fontawesome.min.css">


    <link href="http://sgd.csdl.moet.gov.vn/css/login_master.css" rel="stylesheet" />
    <!-- <link href="http://sgd.csdl.moet.gov.vn/css/bootstrap.min.css" rel="stylesheet" /> -->
    <script src="http://sgd.csdl.moet.gov.vn/js/jquery-2.1.4.min.js"></script>
  </head>
  <body>
      <div class="login">
        <div class="head-bar">
          <div id="banner">
            <table style="width: 100%; padding: 0px; margin: 0px">
              <tr>
                <td style="width: 75px; position: relative; z-index: 10;">
                  <a href="http://csdl.moet.gov.vn/">
                  <img class="img-logo" src="http://csdl.moet.gov.vn/images/logolg.png" /></a>
                </td>
                <td class="title_banner">
                  <label class="sub-text-lg">Bộ giáo dục và đào tạo</label>
                  <label class="text-logo-lg">CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO</label>
                  <label class="text-logo-lg text-logo-lg-mobile" style="display: none;">CƠ SỞ DỮ LIỆU <br/> NGÀNH GIÁO DỤC VÀ ĐÀO TẠO</label>
                  <div class="img-bg-mobile">
                    <img src="http://csdl.moet.gov.vn/images/bg.png" />
                  </div>
                </td>
                <td class="hidden-xs hidden-sm" style="width: 434px; height: 83px; position: relative;">
                  <div class="supportMoet" style="position: absolute; top: 40px; left: 40px;">
                    <p class="hotline" style="color: #fff; padding: 0px; margin: 0px; font-size: 11pt;">HOTLINE: <b>19004740</b></p>
                    <p class="hotline" style="color: #fff; padding: 0px; margin: 0px; padding-top: 3px; font-size: 11pt;">EMAIL HỖ TRỢ: <b>csdl@moet.edu.vn</b></p>
                  </div>
                  <img src="http://csdl.moet.gov.vn/images/bg.png" />
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="content background-page">
          <div class="col-md-6">
            <div class="center-sm">
            {{-- <form class="form-horizontal form-control-line text-center" id="loginform" action="{{ route('login') }}" method="POST"> --}}
              <div>
                <table>
                  <tbody>
                    <tr>
                      <td style="width: 46px">
                        <img src="images/iconlogin.png" id="ContentPlaceHolder1_iconlogin" alt="">
                      </td>
                      <td>
                        <div>
                          <ul style="list-style: none; margin-left: 5px; padding-top: 5px; margin-bottom: initial!important;">
                            <li>
                              <span class="dangnhapht" style="font-size: 22px; color: #666666; text-transform: uppercase;">Đăng nhập hệ thống</span>
                            </li>
                            <li>
                              <span style="font-weight: bold; color: #0095dc;">Quản lý cấp Phòng</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class=" row-space">
                <span style="color: #0086c0; font-size: 14px; text-transform: uppercase;">Tài khoản của bạn</span>
              </div>
              @csrf
              <div class=" row-space">
                <div>
                  <div class="input-group">
                    <input name="ctl00$ContentPlaceHolder1$tbU" type="text" id="ContentPlaceHolder1_tbU" class="form-control input-login" placeholder="Tài khoản đăng nhập" autocomplete="off" style="width:100%;" name="username" value="{{ old('username') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                  </div>
                </div>
              </div>
              <div class=" row-space">
                <div>
                  <div class="input-group">
                    <input name="ctl00$ContentPlaceHolder1$tbP" type="password" id="ContentPlaceHolder1_tbP" class="form-control input-login" placeholder="Mật khẩu" autocomplete="off" style="width:100%;" name="password">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                  </div>
                </div>
              </div>
              <div class=" row-space">
                <span style="color: #0086c0; font-size: 14px; text-transform: uppercase;">Thông tin đơn vị</span>
              </div>
              <div class="row-space">
                <select name="" class="form-control" id="tinhthanh" onchange="get_location(this)">
                  <option value="">Chọn tỉnh thành</option>
                </select>
              </div>
              <div class="row-space">
                <select name=""  class="form-control" id="quanhuyen">
                  <option value="">Chọn quận huyện</option>
                </select>
              </div>
              <div class="btndangnhap row-space">
                <input type="submit" name="ctl00$ContentPlaceHolder1$btOK" value="Đăng nhập" onclick="" id="ContentPlaceHolder1_btOK" class="btn btn-primary btn-login">
              </div>
            {{-- </form> --}}
            </div>
          </div>
        </div>
        <!-- footer -->
        <div style="clear: both; margin-bottom: 60px;" id="top-footer"></div>
        <div class="footer">
          <div id="footer-right">
            <div style="text-align: right"><span class="title-footer">CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO</span></div>
            <div style="float: right; margin-right: 10px;">
              Hotline <span style="color: #9f224e">19004740</span> <span class="hotro-pc">- Hỗ trợ trực tuyến </span><span><a href="http://www.quangich.com/home/vn/upload/info/attach/13113069258761_TeamViewer.rar" style="color: #9f224e; text-decoration: none">Teamviewer</a>
              - <a href="http://dl2.ultraviewer.net/UltraViewer_setup_6.1_vi.exe" style="color: #9f224e; text-decoration: none">Ultraviewer</a></span>
            </div>
          </div>
        </div>
      </div>

      <script>
        function get_location(local){
            var ma = $(local).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });

            if($(local).attr('id')=='tinhthanh'){
                $.ajax({
                    url: "{{ route('get_location') }}",
                    dataType: 'html',
                    data: {matp: ma},
                })
                .done(function(data) {
                    $('#quanhuyen').html(data);
                    $('#quanhuyen > option:first').html('Chọn quận huyện');
                });
            }
        }
        $(function () {
            $.ajax({
                url: "{{ route('get_location') }}",
                dataType: 'html',
                data: {city_check: true}
            })
            .done(function(data) {
                $('#tinhthanh').html(data);
                $('#tinhthanh > option:first').html('Chọn tỉnh thành');
            });
        })
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/perfect-scrollbar.jquery.min.js"></script>
    <!-- plugin js -->


  </body>
</html>