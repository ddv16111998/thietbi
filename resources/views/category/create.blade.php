@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') chuyên mục bài viết @endsection
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') danh mục</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.category.index') }}">@lang('system.manage') danh mục</a></li>
                        <li class="breadcrumb-item active"><a href="">Thêm mới danh mục</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.category.store') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">

                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="title-input" class="col-md-2 text-center col-form-label">Tiêu đề <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input class="form-control " type="text" id="title-input" name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="example-text-input" class="col-md-2 text-center col-form-label">Danh mục cha <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="parent_id" id="" class="form-control">
                                            <option value="0">-- Danh mục gốc --</option>
                                            @php
                                                cat_parent($data)
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="example-text-input" class="col-md-2 text-center col-form-label">Trạng thái</label>
                                    <div class="col-md-5 col-form-label">
                                        <input class="" type="checkbox" id="checkbox-input" name="active">
                                        <label for="checkbox-input">Hoạt động</label>
                                    </div>
                                </div>

                                <section class="text-right">
                                    <div class="row justify-content-around">
                                        <div class="col-md-7 col-sm-8">
                                        <a href="{{ route('admin.category.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
