@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') lớp học @endsection
@section('css')
@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') lớp học</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.classes.index') }}">@lang('system.manage') lớp học</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.classes.create') }}">Thêm mới lớp học</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.classes.store') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Tên lớp học <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="name" value="{{ old('name') }}" id="name" type="text" class="form-control">
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="block_id" class="col-md-2 text-center col-form-label">Khối học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="block_id" id="block_id" class="form-control">
                                            <option value="">---None---</option>
                                        @foreach ($blocks as $block)
                                            <option value="{{ $block->id }}" {{ old('block_id')==$block->id ? 'selected' : '' }}>{{ $block->name }}</option>
                                        @endforeach
                                        </select>
                                        @error('block_id')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="school_id" class="col-md-2 text-center col-form-label">Trường học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="school_id" id="school_id" class="form-control">
                                            <option value="">---None---</option>
                                        @foreach ($schools as $school)
                                            <option value="{{ $school->id }}" {{ old('school_id')==$school->id ? 'selected' : '' }}>{{ $school->name }}</option>
                                        @endforeach
                                        </select>
                                        @error('school_id')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="school_year" class="col-md-2 text-center col-form-label">Năm học <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" class="form-control" name="start" id="start" placeholder="Bắt đầu" value="{{ old('start') }}">
                                            <input type="text" class="form-control" name="end" id="end" placeholder="Kết thúc" value="{{ old('end') }}">
                                        </div>
                                        @error('start')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                        @error('end')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="gvcn_name" class="col-md-2 text-center col-form-label">Tên GVCN <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="gvcn_name" value="{{ old('gvcn_name') }}" id="gvcn_name" type="text" class="form-control">
                                        @error('gvcn_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="gvcn_phone" class="col-md-2 text-center col-form-label">Số điện thoại GVCN <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="gvcn_phone" value="{{ old('gvcn_phone') }}" id="gvcn_phone" type="text" class="form-control">
                                        @error('gvcn_phone')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="gvcn_email" class="col-md-2 text-center col-form-label">Email GVCN <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="gvcn_email" value="{{ old('gvcn_email') }}" id="gvcn_email" type="text" class="form-control">
                                        @error('gvcn_email')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="status" class="col-md-2 text-center col-form-label">Trạng thái </label>
                                    <div class="col-md-5 col-form-label">
                                        <input name="status" id="status" type="checkbox" {{ old('status') ? 'checked' : '' }}> Hoạt động
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="description" class="col-md-2 text-center col-form-label">Mô tả </label>
                                    <div class="col-md-5">
                                        <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.classes.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
