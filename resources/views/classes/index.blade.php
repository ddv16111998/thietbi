@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Lớp học @endsection
@section('css')
@endsection
@section('js')
    <script>
    $(function() {
        $('#listClass').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.classes.index') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'block', name: 'block' },
                { data: 'school', name: 'school' },
                { data: 'gvcn_name', name: 'gvcn_name'},
                { data: 'gvcn_phone', name: 'gvcn_phone'},
                { data: 'gvcn_email', name: 'gvcn_email'},
                { data: 'school_year', name: 'school_year'},
                { data: 'status', name: 'status'},
                { data: 'description', name: 'description'},
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action'}
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        }); 
    });
    </script>
@endsection
@section('content')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">
                    Quản lý lớp học
                </h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.classes.index') }}">Lớp học</a></li>
                </ol>
                <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.classes.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listClass"class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 12px;">ID</th>
                                        <th>Tên lớp học</th>
                                        <th>Khối học</th>
                                        <th>Trường học</th>
                                        <th>GVCN</th>
                                        <th>Số điện thoại</th>
                                        <th>Email</th>
                                        <th>Năm học</th>
                                        <th>Mô tả</th>
                                        <th>Trang thái</th>
                                        <th>@lang('system.created_at')</th>
                                        <th style="width: 120px;">@lang('system.operations')</th>
                                    </tr>
                                    </thead>
                                </table>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
