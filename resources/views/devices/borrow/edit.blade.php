
@extends('theme.master')
@section('title', 'Sửa phiếu mượn thiết bị')
@section('css')
<style>
    input[type=checkbox]
    {
        margin-right: 25px;
    }
</style>
@endsection
@section('js')
    <script>
        $('#search_name').change(function(event) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });
            $.ajax({
                url: "{{ route('admin.liquidation.create') }}",
                data: {device_name: $('#search_name').val()},
            })
                .done(function(data) {
                    $('#modal_table').html(data);
                    $('button.select_device').click(function() {
                        var x = $(this).val();
                        var data = '<div class="form-group row"><input type="hidden" name="device_id[]" value="'+x+'"><div class="col-4 text-right">';
                        data += $(this).attr('data-device');
                        data +='</div><div class="col-4"><input  name="amount[]" type="number" min="0" class="form-control" placeholder="Số lượng mượn" /></div><div class="col-4"><input type="button" value="X" class="btn text-danger" onclick="rm(this)"/></div></div>';
                        $('#btnAdd').before(data);
                    });
                });
        });

        $('#listType').change(function (event) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });
            $.ajax({
                url: "{{route('admin.liquidation.create')}}",
                data:{ typeData: $('#listType').val() },
            })
                .done(function(data) {
                    $('#modal_table').html(data);
                    $('button.select_device').click(function() {
                        var x = $(this).val();
                        var data = '<div class="form-group row"><input type="hidden" name="device_id[]" value="'+x+'"><div class="col-4 text-right">';
                        data += $(this).attr('data-device');
                        data +='</div><div class="col-4"><input  name="amount[]" type="number" min="0" class="form-control" placeholder="Số lượng mượn" /></div><div class="col-4"><input type="button" value="X" class="btn text-danger" onclick="rm(this)"/></div></div>';
                        $('#btnAdd').before(data);
                    });
                })
        });

        $('button.select_device').click(function () {
            var x = $(this).val();
            var data = '<div class="form-group row"><input type="hidden" name="device_id[]" value="'+x+'"><div class="col-4 text-right">';
            data += $(this).attr('data-device');
            data +='</div><div class="col-4"><input  name="amount[]" type="number" min="0" class="form-control" placeholder="Số lượng mượn" /></div><div class="col-4"><input type="button" value="X" class="btn text-danger" onclick="rm(this)"/></div></div>';
            $('#btnAdd').before(data);
        })
        function rm(obj) {
            var x = $(obj).parent();
            x.remove();
        }
    </script>
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>
    <script>
        function get_info(info){
            var ma = $(info).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });

            if($(info).attr('id')=='school_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {school_id: ma},
                })
                    .done(function(data) {
                        $('#block_id').html(data);
                    });
                $('#classes_id').html('<option>--None--</option>');
            }
            if($(info).attr('id')=='block_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {block_id: ma},
                })
                    .done(function(data) {
                        $('#classes_id').html(data);
                    });
            }
        }


        $(function () {
            $.ajax({
                url: "{{ route('get_info') }}",
                dataType: 'html',
                data: {school_check: true}
            })
                .done(function(data) {
                    $('#school_id').html(data);
                });
        })
    </script>
@endsection
@section('content')

    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Sửa phiếu mượn thiết bị</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.devices.index') }}">@lang('system.manage') thiết bị </a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.borrow-and-return.index') }}">Danh sách mượn trả thiết bị </a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.borrow-and-return.create') }}">Sửa phiếu mượn trả thiết bị </a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.borrow-and-return.update',$deviceBorrow['id']) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    @if ($errors->any())
                                      <div class="alert alert-danger">
                                          <ul>
                                              @foreach ($errors->all() as $error)
                                                  <li>{{ $error }}</li>
                                              @endforeach
                                          </ul>
                                      </div>
                                    @endif
                                </div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @php
                                                $time_borrow = explode("-",$deviceBorrow['time_borrow']);
                                            @endphp
                                            <div class="form-group">
                                                <label>Ngày mượn<span class="text-danger">*</span></label>
                                                <input type="text" name="time_borrow" class="form-control form-control-line datepicker" value="{{ $time_borrow[2].'/'.$time_borrow[1].'/'.$time_borrow[0] }}">
                                            </div>
                                            @php
                                                $time_return = explode("-",$deviceBorrow['time_return']);
                                            @endphp
                                            <div class="form-group">
                                                <label>Ngày trả<span class="text-danger">*</span></label>
                                                <input type="text" name="time_return" class="form-control form-control-line datepicker" value="{{ $time_return[2].'/'.$time_return[1].'/'.$time_return[0] }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Giáo viên mượn<span class="text-danger">*</span></label>
                                                <input type="text" name="teacher" class="form-control form-control-line" value="{{ $deviceBorrow['teacher'] }}">
                                            </div>

                                            <div class="form-group">
                                                <label>Môn học<span class="text-danger">*</span></label>
                                                <select name="subject_id" id="subject" class="form-control">
                                                    <option value="">--Chọn môn học--</option>
                                                    @foreach($subjects as $subject)
                                                        <option value="{{$subject->id}}" {{ $deviceBorrow['discipline']==$subject->id ? 'selected' : '' }}>{{$subject->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="school_id" class="">Trường học<span class="text-danger">*</span></label>
                                                <select name="school_id" id="school_id" class="form-control" onchange="get_info($(this))">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="block_id" class="">Khối học<span class="text-danger">*</span></label>
                                                <select name="block_id" id="block_id" class="form-control" onchange="get_info($(this))">
                                                    <option value="">--- Chọn khối học ---</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Là bài thực hành? <span class="text-danger">*</span></label><br>
                                                <input type="checkbox" name="is_practice" value="true" @if($deviceBorrow['is_practice']=="1") checked @endif >
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Lớp<span class="text-danger">*</span></label>
                                                <select name="classes_id" id="classes_id" class="form-control">
                                                    <option value="">-- Chọn lớp --</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Năm học <span class="text-danger">*</span></label>
                                                <select name="school_year_id" id="school_year_id" class="form-control form-control-line">
                                                    <option value="">-- Chọn năm học --</option>
                                                    @foreach ($school_years as $school_year)
                                                        <option value="{{ $school_year->id }}" @if($deviceBorrow['school_year_id']==$school_year->id) selected @endif>{{ $school_year->school_year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Khối lượng</label>
                                                <input type="text" name="mass" class="form-control form-control-line" value="{{ $deviceBorrow['mass'] }}">
                                            </div>
                                            {{--                                        <div class="form-group">--}}
                                            {{--                                            <label>Lớp<span class="text-danger">*</span></label>--}}
                                            {{--                                            <select name="classes_id[]" id="class" class="form-control" multiple>--}}
                                            {{--                                                <option value="">-- Chọn lớp --</option>--}}
                                            {{--                                                @foreach($class as $value)--}}
                                            {{--                                                    <option value="{{$value['id']}}" @if(in_array($value['id'],$deviceBorrow['classes_id'])) selected @endif>{{$value['name']}}</option>--}}
                                            {{--                                                @endforeach--}}
                                            {{--                                            </select>--}}
                                            {{--                                        </div>--}}

                                            <div class="form-group">
                                                <label>Tên bài dạy <span class="text-danger">*</span></label><br>
                                                <input type="text" name="lesson_name" value="{{ $deviceBorrow['lesson_name'] }}" class="form-control form-control-line">
                                            </div>

                                            <div class="form-group">
                                                <label>Dạy tiết<span class="text-danger">*</span></label><br>

                                                <label for="">1</label>
                                                <input type="checkbox" name="lesson_num[1]" value="1" @if(in_array('1',$deviceBorrow['lesson_num'])) checked @endif>
                                                <label for="">2</label>
                                                <input type="checkbox" name="lesson_num[2]" value="2" @if(in_array('2',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">3</label>
                                                <input type="checkbox" name="lesson_num[3]" value="3" @if(in_array('3',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">4</label>
                                                <input type="checkbox" name="lesson_num[4]" value="4" @if(in_array('4',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">5</label>
                                                <input type="checkbox" name="lesson_num[5]" value="5" @if(in_array('5',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">6</label>
                                                <input type="checkbox" name="lesson_num[6]" value="6" @if(in_array('6',$deviceBorrow['lesson_num'])) checked @endif>
                                                <label for="">7</label>
                                                <input type="checkbox" name="lesson_num[7]" value="7" @if(in_array('7',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">8</label>
                                                <input type="checkbox" name="lesson_num[8]" value="8" @if(in_array('8',$deviceBorrow['lesson_num'])) checked @endif>

                                                <label for="">9</label>
                                                <input type="checkbox" name="lesson_num[9]" value="9" @if(in_array('9',$deviceBorrow['lesson_num'])) checked @endif>

                                            </div>
                                            <div class="form-group">
                                                <label>Số tiết sử dụng<span class="text-danger">*</span></label><br>
                                                <input type="number" name="lesson_use" value="{{ $deviceBorrow['lesson_use'] }}" min="0" class="form-control form-control-line">
                                            </div>
                                            
                                        </div>
                                    </div>
                                      <div class="col-12">
                                        <div class="form-group border">
                                            <label class="text-muted">Chọn thiết bị: <span class="text-danger">(*)</span></label>
                                            @foreach ($deviceBorrow->device()->get() as $device)
                                            <div class="form-group row">
                                                <input type="hidden" name="device_id[]" value="{{ $device->id }}" />
                                                <div class="col-4 text-right">
                                                    {{ $device->name }}
                                                </div>
                                                <div class="col-4"><input name="amount[]" type="number" min="0" class="form-control" placeholder="0" value="{{ $device->pivot->amount }}" /></div>
                                                <div class="col-4">
                                                    <input type="button" value="X" class="btn text-danger" onclick="rm(this)">
                                                </div>
                                                </div>
                                                @endforeach
                                            @error('device_id')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                            <input type="button" value="+ Thêm thiết bị" class="btn form-control" id="btnAdd" data-toggle="modal" data-target="#myModal">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                @php
                                                    $return_date = explode("-",$deviceBorrow['return_date']);
                                                @endphp
                                                <label for="">Ngày trả thiết bị</label>
                                                <input type="text" name="return_date" class="form-control form-control-line datepicker" value="{{ $deviceBorrow['return_date'] ? $return_date[2].'/'.$return_date[1].'/'.$return_date[0] : '' }}" placeholder="Chọn ngày trả thiết bị">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Trạng thái</label>
                                                <select name="status" id="" class="form-control">
                                                    <option value="">-- Chọn trạng thái --</option>
                                                    <option value="0" @if($deviceBorrow->status == 0) selected @endif>Đang sử dụng</option>
                                                    <option value="1" @if($deviceBorrow->status == 1) selected @endif>Đã trả</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Tình trạng sau khi trả</label>
                                            <input type="text" name="state" value="{{$deviceBorrow->state}}" class="form-control form-control-line" placeholder="Nhập tình trạng sau khi trả">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Chú thích</label>
                                                <textarea class="form-control form-control-line ckeditor" name="note" id="note">
                                                {{ $deviceBorrow['note'] }}
                                            </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.borrow-and-return.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
      <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Chọn thiết bị</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Chọn kho thiết bị</label>
                                <select name="" id="cate_id" class="form-control">
                                    <option value="">--None--</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tên thiết bị</label>
                                <input type="text" class="form-control" placeholder="tìm kiếm tên..." id="search_name">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Chọn thiết bị từ danh sách: </label>
                                <select name="type" id="listType" class="form-control">
                                    <option value="">-- Chọn danh sách --</option>
                                    <option value="0">Hỏng/mất thiết bị</option>
                                    <option value="1">Thiết bị đang sử dụng</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h3>Thiết bị</h3>
                    <table class="table">
                        <tr>
                            <th>Số hiệu</th>
                            <th>Mã thiết bị</th>
                            <th>Tên thiết bị</th>
                            <th>Đơn vị</th>
                            <th width="12px">Chọn</th>
                        </tr>
                        <tbody id="modal_table">
                        @foreach ($devices as $device)
                            <tr>
                                <td>{{ $device->so_hieu }}</td>
                                <td>{{ $device->code }}</td>
                                <td>{{ $device->name }}</td>
                                <td>{{ $device->unit }}</td>
                                <td>
                                    <button class="btn btn-info select_device" value="{{ $device->id }}" data-device="{{ $device->name }}" title="chọn">>>></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" value="Xong" class="btn btn-info" data-dismiss="modal">
                </div>
            </div>

        </div>
    </div>
@endsection
