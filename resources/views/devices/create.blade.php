@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Thêm mới thiết bị')
@section('css')

@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>

    <script>
        function get_info(info){
            var ma = $(info).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });

            if($(info).attr('id')=='school_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {school_id: ma},
                })
                    .done(function(data) {
                        $('#block_id').html(data);
                    });
                $('#classes_id').html('<option>--None--</option>');
            }
            if($(info).attr('id')=='block_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {block_id: ma},
                })
                    .done(function(data) {
                        $('#classes_id').html(data);
                    });
            }
        }
        $(function () {
            $.ajax({
                url: "{{ route('get_info') }}",
                dataType: 'html',
                data: {school_check: true}
            })
                .done(function(data) {
                    $('#school_id').html(data);
                });
        })
    </script>
    <script>
        $(function () {
            $('#to_money').click(function () {
                var amount = $('#amount').val();
                var price = $('#price').val();
                $('#to_money').val(amount*price);
            })
        })
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Thêm mới</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.devices.index') }}">Quản lý thiết bị</a></li>
                        <li class="breadcrumb-item active"><a href="">Thêm mới</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.devices.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    @if ($errors->any())
                                      <div class="alert alert-danger">
                                          <ul>
                                              @foreach ($errors->all() as $error)
                                                  <li>{{ $error }}</li>
                                              @endforeach
                                          </ul>
                                      </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
{{--                                    <div class="col-sm-6">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Ảnh thiết bị</label>--}}
{{--                                            <div>--}}
{{--                                                <img src="/images/no-image-icon.png" class="img_change" onclick="$('.inp__img').click()" style="width:200px;cursor:pointer;">--}}
{{--                                                <input type="file" name="image" class="inp__img hide">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-sm-6">--}}
                                        <div class="form-group">
                                            <label>Tên thiết bị<span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control form-control-line" value="{{ old('name') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Mã thiết bị<span class="text-danger">*</span></label>
                                            <input type="text" name="code" class="form-control form-control-line" value="{{ old('code') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Số hiệu</label>
                                            <input type="text" name="so_hieu" class="form-control form-control-line" value="{{ old('so_hieu') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Quy cách</label>
                                            <input type="text" name="quy_cach" class="form-control form-control-line" value="{{ old('quy_cach') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="school_year" class="">Năm học<span class="text-danger">*</span></label>
                                            <select name="school_year_id" id="school_year_id" class="form-control">
                                                <option value=""> --Chọn năm học--</option>
                                                @foreach($school_years as $school_year)
                                                        <option value="{{$school_year->id}}">{{$school_year->school_year}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="school_id" class="">Trường học<span class="text-danger">*</span></label>
                                                <select name="school_id" id="school_id" class="form-control" onchange="get_info($(this))">
                                                    <option value="">--- Chọn trường học ---</option>
                                                </select>
                                                @error('school_id')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="block_id" class="">Khối học<span class="text-danger">*</span></label>
                                                <select name="block_id" id="block_id" class="form-control" onchange="get_info($(this))">
                                                    <option value="">--- Chọn khối học ---</option>
                                                </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Khối lớp<span class="text-danger">*</span></label>
                                            <select name="classes_id[]" id="classes_id" class="form-control" multiple>
                                                <option value="">-- Chọn khối lớp --</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="">Danh mục<span class="text-danger">*</span></label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <option value="">--- Chọn danh muc ---</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Kho/phòng bộ môn<span class="text-danger">*</span></label>
                                            <select name="classroom_id[]" class="form-control" id="inputSltCate" multiple>
                                                <option value="0">-- Chọn phòng học --</option>
                                                @foreach ($classrooms as $val)
                                                    <option value="{{ $val->id }}"> {{ $val->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Đơn vị</label>
                                            <input type="text" name="unit" class="form-control form-control-line" value="{{ old('unit') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input type="number" id="amount" name="amount" class="form-control form-control-line" value="{{ old('amount') }}" min=0>
                                        </div>
                                        <div class="form-group">
                                            <label>Đơn giá</label>
                                            <input type="number" id="price" name="price" class="form-control form-control-line" value="{{ old('price') }}" min=0>
                                        </div>
                                        <div class="form-group">
                                            <label>Thành tiền</label>
                                            <input type="text" name="to_money" id="to_money" class="form-control form-control-line" value="{{ old('to_money') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Ngày đưa vào SD</label>
                                            <input type="text" name="time_use" class="form-control form-control-line datepicker" placeholder="dd/mm/yyyy" value="{{old('time_use')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Năm học đưa vào SD</label>
                                            <input type="text" name="year_use" class="form-control form-control-line date-own" value="{{old('year_use')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label>Mô tả<span class="text-danger">*</span></label>
                                        <textarea  name="desciption" class="form-control form-control-line ckeditor">
                                            {{old('desciption')}}
                                        </textarea>
                                    </div>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.devices.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
