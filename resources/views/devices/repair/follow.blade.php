@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Theo dõi sửa chữa thiết bị')
@section('css')
@endsection
@section('js')
    <script>
        $(function() {
            $('#listUsers').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.repair.follow',$id) }}',
                columns: [
                    { data: 'stt', name: 'stt' },
                { data: 'repair_id', name: 'repair_id' },
                { data: 'device_id', name: 'device_id' },
                { data : 'amount', name: 'amount'},
                { data: 'price', name: 'price' },
                { data: 'total_price', name: 'total_price' },
                { data: 'status', name: 'status' },
                    {data:'update_status',name:'update_status'}
                ],
                language : {
                    "emptyTable":     "Không có dữ liệu trong bảng",
                    "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                    "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                    "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                    "thousands":      ",",
                    "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                    "loadingRecords": "Chờ xíu nhé...",
                    "processing":     "Đang xử lí...",
                    "search":         "Tìm kiếm:",
                    "zeroRecords":    "Không tìm thấy kết quả",
                    "paginate": {
                        "first":      "Đầu",
                        "last":       "Cuối",
                        "next":       "Tiếp",
                        "previous":   "Trước"
                    }
                }
            });
        });
    </script>
    <script>
        function updateStatus(id) {
            var csrf = '@csrf';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': csrf.value
                }
            });
            $.ajax({
                'url' : '{{ route('admin.repair.index') }}',
                'data': {
                    'id' : id,
                },
                success: function (data) {
                    console.log(data);
                    $('#a').attr('href', "{{route('admin.repair.update-status')}}/"+data+"/0");
                    $('#b').attr('href', "{{route('admin.repair.update-status')}}/"+data+"/1");
                    $('#c').attr('href', "{{route('admin.repair.update-status')}}/"+data+"/2");
                    $('#d').attr('href', "{{route('admin.repair.update-status')}}/"+data+"/3");
                }
            });
        }
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Theo dõi sửa chữa thiết bị</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.devices.index') }}">Quản lý thiết bị</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.repair.index') }}">Sữa chữa thiết bị</a></li>
                        <li class="breadcrumb-item active"><a href="">Theo dõi sửa chữa thiết bị</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <table class="table table-hover mt-4" id="listUsers">
                                <thead>
                                <th>Stt</th>
                                <th>Tên phiếu</th>
                                <th>Tên thiết bị</th>
                                <th>Số lượng sửa</th>
                                <th>Đơn giá sửa</th>
                                <th>Tổng tiền</th>
                                <th>Trạng thái sửa</th>
                                <th>Cập nhật trạng thái sửa</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div id="updateStatus" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-left"><span id="name"></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="justify-content-md-center">
                            <div class="col-md-12 row form-group">
                               <h3>Cập nhật trạng thái sửa chữa</h3>
                            </div><hr>
                            <div class="row form-group">
                                <div class="col-3">
                                    <a class="btn btn-warning button" id="a" name="" href=""><i class="fa fa-chain-broken"></i> Cần sửa chữa</a>
                                </div>
                                <div class="col-3">
                                    <a class="btn btn-primary" id="b" name="" href=""><i class="fa fa-wrench "></i> Đang sửa chữa</a>
                                </div>
                                <div class="col-3">
                                    <a class="btn btn-success" id="c" name="" href=""><i class="fa fa-check"></i> Đã sửa chữa xong</a>
                                </div>
                                <div class="col-3">
                                    <a class="btn btn-danger" id="d" name="" href=""><i class="fas fa-exclamation-triangle"></i> Không sửa chữa được</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@endsection
