<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.dashboard') }}">
                        <i class="icon-speedometer"></i><span class="hide-menu">Dashboard</span>
                    </a>
                    <br>
                </li>

                <li class="nav-small-cap">--- NHÂN SỰ</li>
                <!--Nhân Sự-->
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="icon-people"></i><span class="hide-menu">Nhân Viên</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.user.index') }}">QL Nhân viên</a></li>
                        <li><a href="{{ route('admin.role.index') }}">@lang('acl.role_name_module')</a></li>
                    </ul>

                </li>

                <li class="nav-small-cap">--- TRƯỜNG HỌC</li>
                <!--Trường học-->
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.school.index') }}">
                        <i class="fa fa-home"></i><span class="hide-menu">Trường học</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.block.index') }}" >
                        <i class="fa fa-th-large"></i><span class="hide-menu">Khối lớp</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.classes.index') }}">
                        <i class="fa fa-th"></i><span class="hide-menu">Lớp học</span>
                    </a>
                </li>

                <li class="nav-small-cap">--- NĂM HỌC</li>
                <!--Năm học-->
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.school-year.index')}}">
                        <i class="fa fa-calendar-alt"></i><span class="hide-menu">Năm học</span>
                    </a>
                </li>


                <li class="nav-small-cap">--- THIẾT BỊ</li>
                <!--Thiết Bị-->
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.devices.index') }}">
                        <i class="fa fa-puzzle-piece"></i><span class="hide-menu">Thiết Bị</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.classroom.index') }}">
                        <i class="fa fa-circle-o" aria-hidden="true"></i><span class="hide-menu">Phòng bộ môn</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.borrow-and-return.index') }}">
                        <i class="fa fa-circle-o"></i><span class="hide-menu">Mượn trả</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.category.index') }}">
                        <i class="fas fa-ellipsis-v"></i><span class="hide-menu">Danh mục</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.subject.index') }}">
                        <i class="fas fa-book"></i><span class="hide-menu">Môn học</span>
                    </a>
                </li>

                <li class="nav-small-cap">--- BÁO CÁO</li>
                <!--Báo Cáo-->
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="ti-bar-chart"></i><span class="hide-menu">Báo Cáo</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="#">Báo Cáo Doanh Thu</a></li>
                        <li><a href="#">Báo Cáo Lợi Nhuận</a></li>
                        <li><a href="#">Báo Cáo Chi Lương</a></li>
                        <li><a href="#">Báo Cáo Chi Vật Tư</a></li>
                        <li><a href="#">Báo Cáo Chi MKT</a></li>
                    </ul>
                </li>

                <li class="nav-small-cap">--- HỆ THỐNG</li>
                <!--Hỗ Trợ-->
                <li>
                    <a class="waves-effect waves-dark" href="pages-faq.html" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">FAQs</span>
                    </a>
                </li>

                <!--Hệ Thống-->
                <li>
                    <a class="waves-effect waves-dark" href="{{ route('admin.diary.index') }}">
                        <i class="ti-settings"></i>
                        <span class="hide-menu">System</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" aria-expanded="false">
                        <i class="icon-power text-danger"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
