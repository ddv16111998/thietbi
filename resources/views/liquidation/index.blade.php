@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Thanh lý thiết bị @endsection
@section('css')
@endsection
@section('js')
    <script>
        $('#listLiquidation').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.liquidation.index') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name'},
                { data: 'device', name: 'device'},
                { data: 'description', name: 'description'},
                { data: 'action', name: 'action'}
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    </script>

    <script>
        $('#export').click(function () {
            $('#form-export').submit();
        });
    </script>
@endsection
@section('content')
    @include('layouts.nav-tabs')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Thanh lý thiết bị
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.liquidation.index') }}">Thanh lý thiết bị</a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.liquidation.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row text-right"><a  href="" target=_self accesskey="x" class="btn btn-outline-success" data-toggle="modal" data-target="#myModal">Xuất Excel</a></div>
                                <form id="form-list-crud" action="/" method="POST">
                                    @csrf
                                    <table id="listLiquidation" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="width: 12px;">ID</th>
                                            <th>Tên phiếu</th>
                                            <th>Thiết bị</th>
                                            <th>Ghi chú</th>
                                            <th style="width: 120px;">@lang('system.operations')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



{{--    <!-- Modal -->--}}
{{--    <div id="myModal" class="modal fade" role="dialog">--}}
{{--        <div class="modal-dialog modal-sm">--}}

{{--            <!-- Modal content-->--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <div class="row form-group">--}}
{{--                        <div class="col-4">--}}
{{--                            <label>Chọn năm: </label>--}}
{{--                        </div>--}}
{{--                        <div class="col-8">--}}
{{--                            <form action="{{ route('admin.devicedown.excel') }}" id="form-export">--}}
{{--                                <select name="" id="" class="form-control">--}}
{{--                                    <option value="">2018-2019</option>--}}
{{--                                    <option value="">2019-2020</option>--}}
{{--                                    <option value="">2020-2021</option>--}}
{{--                                    <option value="">2021-2022</option>--}}
{{--                                </select>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-success" id="export">Xuất excel</button>--}}
{{--                    <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ</button>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
@endsection
