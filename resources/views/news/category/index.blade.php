@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.manage') chuyên mục @endsection
@section('css')
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" /> --}}
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@endsection
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    function getDataTable(url, columnDefs, arrColumns) {
        var columns = [];
        $.each(arrColumns, function( index, value ) {
            columns.push({data: value, name: value});
        });

        $('#news').DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: columns,
            "columnDefs": columnDefs,
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    }

    $(function() {
        var url = '{!! route('admin.anydata') !!}';
        var columnDefs = [
            { "orderable": true, "targets": 1 }
        ];
        var arrColumns = ['id', 'title', 'intro'];
        getDataTable(url, columnDefs, arrColumns);

    });

</script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        @lang('system.manage') chuyên mục
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.newscate')}}">@lang('system.manage') chuyên mục</a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{route('admin.newscate.create')}}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <form id="form-list-crud" action="" method="POST">
                                    @csrf
                                    <table id="news" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            {{-- <th style="width: 12px;" class="check-all">
                                                <input type="checkbox" class="check" id="checkAll">
                                            </th> --}}
                                            {{-- <th style="width: 12px;">#</th> --}}
                                            <th>id</th>
                                            <th>Tiêu đề</th>
                                            <th style="width: 120px;">Mô tả</th>
                                            {{-- <th></th> --}}
                                        </tr>
                                        </thead>
                                        {{-- <tbody> --}}
                                        {{-- @if(sizeof($items) > 0)
                                            @foreach ($items as $item)
                                                <tr>
                                                    <td class="text-center">
                                                        <input name="item[]" type="checkbox" value="{{$item->id}}" class="check checkItem">
                                                    </td>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td class="btn-option">
                                                        <div class="btn-group">
                                                            <a
                                                                href="{{route('app.{{modelNameSingularLowerCase}}.edit', [$item->id])}}"
                                                                class="btn btn-default-custom btn-show-lang text-left pl-2"
                                                                title="@lang('system.edit')"
                                                            >
                                                                @lang('system.edit') <i class="ti-pencil"></i>
                                                            </a>
                                                            <span class="btn-translate btn btn-default-custom dropdown-toggle dropdown-toggle-split"
                                                                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                                            >
                                                                <span class="sr-only btn-default-custom">Toggle Dropdown</span>
                                                            </span>
                                                            <div class="dropdown-menu">
                                                                <a
                                                                    onclick="return  confirm('@lang('system.do_you_want_to')' +' '+ ' @lang('system.delete')')"
                                                                    href="{{route('app.{{modelNameSingularLowerCase}}.destroy', [$item->id])}}"
                                                                    class="text-left pl-2"
                                                                    title="@lang('system.delete')"
                                                                >
                                                                    @lang('system.delete') <i class="ti-trash"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><div class="alert alert-danger text-center">@lang('system.no_data_matching').</div></td>
                                            </tr>
                                        @endif --}}
                                        {{-- </tbody> --}}
                                    </table>
                                    
                                    <div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
