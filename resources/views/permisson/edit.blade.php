@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') Cập nhật quyền @endsection
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Cập nhật quyền</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.permisson') }}">@lang('system.manage') quyền</a></li>
                        <li class="breadcrumb-item active"><a href="">Cập nhật quyền</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.permisson.update') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <input type="hidden" name="id" value="{{ $permisson->id }}">
                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="title-input" class="col-md-2 text-center col-form-label">Tên quyền <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input class="form-control " type="text" id="title-input" name="display_name" value="{{ $permisson->display_name }}">
                                        @error('display_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="permisson-input" class="col-md-2 text-center col-form-label">Route <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input class="form-control " type="text" id="permisson-input" name="route_permisson" value="{{ $permisson->route }}">
                                        @error('route_permisson')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="note-input" class="col-md-2 text-center col-form-label">Ghi chú </label>
                                    <div class="col-md-5">
                                        <textarea class="form-control" id="note-input" name="note">{{ $permisson->note }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="text-input" class="col-md-2 text-center col-form-label">Trạng thái</label>
                                    <div class="col-md-5 col-form-label">
                                        <input class="" type="checkbox" id="checkbox-input" name="status" {{ ($permisson->status==1) ? "checked" : "" }}>
                                        <label for="checkbox-input">Hoạt động</label>
                                    </div>
                                </div>

                                <section class="text-right">
                                    <div class="row justify-content-around">
                                        <div class="col-md-7 col-sm-8">
                                        <a href="{{ route('admin.permisson') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
