@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') Vai trò @endsection
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') vai trò</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.role.index') }}">@lang('system.manage') vai trò</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.role.create') }}">Thêm mới vai trò</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.role.store') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="title-input" class="col-md-2 text-center col-form-label">Tên vai trò <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input class="form-control" required type="text" id="title-input" name="name" value="{{ old('name') }}">
                                        @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-md-12" style="padding-bottom:20px">
                                        <strong>Quyền</strong>
                                    </div>
                                    @foreach ($route_name as $permisson)
                                        <div class="col-md-3">
                                            <div class="col-md-5 col-form-label">
                                                <input value="{{ $permisson['route'] }}" type="checkbox" id="checkbox-input" name="permisson[]">
                                                <label for="checkbox-input">{{ $permisson['display_name'] }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.user.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
