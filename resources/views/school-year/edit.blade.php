@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') Sửa năm học @endsection
@section('css')
@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Sửa năm học</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.classes.index') }}">@lang('system.manage') năm học</a></li>
                        <li class="breadcrumb-item active"><a href="#">Sửa năm học</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{route('admin.school-year.update',$data->id)}}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                @php
                                    $year = explode('-',$data->school_year);
                                @endphp
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Năm học <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" class="form-control" name="start" id="start" placeholder="Bắt đầu" value="{{$year[0]}}">
                                            <input type="text" class="form-control" name="end" id="end" placeholder="Kết thúc" value="{{ $year[1] }}">
                                        </div>
                                        @error('start')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                        @error('end')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @php
                                    $start_HK1 = explode('-',$data->start_HK1);
                                @endphp
                                <div class="form-group row justify-content-md-center">
                                    <label for="" class="col-md-2 text-center col-form-label">Ngày bắt đầu học kì 1<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input type="text" name="start_HK1" class="form-control form-control-line datepicker" placeholder="dd/mm/yyyy" value="{{$start_HK1[2].'/'.$start_HK1[1].'/'.$start_HK1[0]}}">
                                        @error('start_HK1')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                @php
                                    $start_HK2 = explode('-',$data->start_HK2);
                                @endphp
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Ngày bắt đầu học kì 2<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input type="text" name="start_HK2" class="form-control form-control-line datepicker" placeholder="dd/mm/yyyy" value="{{$start_HK2[2].'/'.$start_HK2[1].'/'.$start_HK2[0]}}">
                                        @error('start_HK2')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>


                                @php
                                    $end_year = explode('-',$data->end_year);
                                @endphp
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Ngày kết thúc năm học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input type="text" name="end_year" class="form-control form-control-line datepicker" placeholder="dd/mm/yyyy" value="{{$end_year[2].'/'.$end_year[1].'/'.$end_year[0]}}">
                                        @error('end_year')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.school-year.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
