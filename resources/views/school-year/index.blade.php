@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Năm học @endsection
@section('css')
@endsection
@section('js')
    <script>
        $(function() {
            $('#listSchool-Years').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.school-year.index') !!}',
                columns: [
                    { data: 'stt', name: 'stt' },
                    { data: 'school_year', name: 'school_year' },
                    { data: 'start_HK1', name: 'start_HK1' },
                    { data: 'start_HK2', name: 'start_HK2' },
                    { data: 'end_year', name: 'end_year'},
                    { data: 'action', name: 'action'}
                ],
                language : {
                    "emptyTable":     "Không có dữ liệu trong bảng",
                    "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                    "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                    "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                    "thousands":      ",",
                    "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                    "loadingRecords": "Chờ xíu nhé...",
                    "processing":     "Đang xử lí...",
                    "search":         "Tìm kiếm:",
                    "zeroRecords":    "Không tìm thấy kết quả",
                    "paginate": {
                        "first":      "Đầu",
                        "last":       "Cuối",
                        "next":       "Tiếp",
                        "previous":   "Trước"
                    }
                }
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Năm học
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.school-year.index') }}">Năm học</a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.school-year.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="form-list-crud" action="/" method="POST">
                                    @csrf
                                    <table id="listSchool-Years"class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Stt</th>
                                            <th>Năm học</th>
                                            <th>Ngày bắt đầu học kì I</th>
                                            <th>Ngày bắt đầu học kì II</th>
                                            <th>Ngày kết thúc năm học</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                    </table>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
