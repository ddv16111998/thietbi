@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') Trường học @endsection
@section('css')
@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>
    <script>
        function get_location(local){
            var ma = $(local).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });

            if($(local).attr('id')=='city'){
                $.ajax({
                    url: "{{ route('get_location') }}",
                    dataType: 'html',
                    data: {matp: ma},
                })
                .done(function(data) {
                    $('#district').html(data);
                });
                $('#commune').html('<option>--None--</option>');
            }
            if($(local).attr('id')=='district'){
                $.ajax({
                    url: "{{ route('get_location') }}",
                    dataType: 'html',
                    data: {maqh: ma},
                })
                .done(function(data) {
                    $('#commune').html(data);
                });
            }
        }
        $(function () {
            $.ajax({
                url: "{{ route('get_location') }}",
                dataType: 'html',
                data: {city_check: true}
            })
            .done(function(data) {
                $('#city').html(data);
            });
        })
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') trường học</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.school.index') }}">@lang('system.manage') trường học</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.school.create') }}">Thêm mới trường học</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.school.store') }}" enctype="multipart/form-data">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="justify-content-md-center">
                                    <div class="col-md-12 row form-group">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Maps</label>
                                                <div>
                                                    <img src="/images/no-image-icon.png" data-id="img_maps" onclick="$('#img_maps').click();" style="width:200px;cursor:pointer;">
                                                    <input type="file" id="img_maps" name="image_maps" class="inp__img hide" onchange="image_change(this)">
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Thời khóa biểu</label>
                                                <div>
                                                    <img src="/images/no-image-icon.png" data-id="img_schedule" onclick="$('#img_schedule').click();" style="width:200px;cursor:pointer;">
                                                    <input type="file" id="img_schedule" onchange="image_change(this)" name="image_schedule" class="hide">
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Tên trường học <span class="text-danger">*</span></label>
                                                <input name="name" value="{{ old('name') }}" id="name" type="text" class="form-control">
                                                @error('name')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <label for="city">Thành phố </label>
                                                    <select name="city" id="city" class="form-control" onchange="get_location($(this));">
                                                    </select>
                                                    @error('city')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="district">Huyện </label>
                                                    <select name="district" id="district" onchange="get_location($(this));" class="form-control">
                                                        <option value="">--None--</option>
                                                    </select>
                                                    @error('district')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="commune">Xã </label>
                                                    <select name="commune" id="commune" class="form-control">
                                                        <option value="">--None--</option>
                                                    </select>
                                                    @error('commune')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Địa chỉ <span class="text-danger">*</span></label>
                                                <input name="address" value="{{ old('address') }}" id="address" type="text" class="form-control">
                                                @error('address')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email <span class="text-danger">*</span></label>
                                                <input name="email" value="{{ old('email') }}" id="email" type="text" class="form-control">
                                                @error('email')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="hotline">Hotline <span class="text-danger">*</span></label>
                                                <input name="hotline" value="{{ old('hotline') }}" id="hotline" type="text" class="form-control">
                                                @error('hotline')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <label for="level">Cấp <span class="text-danger">*</span></label>
                                                    <select name="level" id="level" class="form-control">
                                                        <option value="1" {{ old('level')==1 ? 'selected' : '' }}>Cấp 1</option>
                                                        <option value="2" {{ old('level')==2 ? 'selected' : '' }}>Cấp 2</option>
                                                        <option value="3" {{ old('level')==3 ? 'selected' : '' }}>Cấp 3</option>
                                                    </select>
                                                    @error('level')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="">Năm học <span class="text-danger">(*)</span></label>
                                                <select name="year_link" id="year_link" class="form-control">
                                                    <option value="">-- Chọn năm học --</option>
                                                    @foreach($school_years as $school_year)
                                                        <option value="{{$school_year->id}}" @if(old('year_link')== $school_year->id) selected @endif>{{$school_year->school_year}}</option>
                                                    @endforeach
                                                </select>
                                                @error('year_link')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="principal_name">Tên hiệu trưởng <span class="text-danger">*</span></label>
                                                <input name="principal_name" value="{{ old('principal_name') }}" id="principal_name" type="text" class="form-control">
                                                @error('principal_name')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="principal_birthday">Ngày sinh hiệu trưởng </label>
                                                <input name="principal_birthday" value="{{ old('principal_birthday') }}" id="principal_birthday" type="text" class="datepicker form-control" placeholder="dd/mm/yyyy">
                                                @error('principal_birthday')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="principal_phone">Số điện thoại hiệu trưởng </label>
                                                <input name="principal_phone" value="{{ old('principal_phone') }}" id="principal_phone" type="text" class="form-control">
                                                @error('principal_phone')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="principal_email">Email hiệu trưởng </label>
                                                <input name="principal_email" value="{{ old('principal_email') }}" id="principal_email" type="text" class="form-control">
                                                @error('principal_email')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="vice_principal_name">Tên hiệu phó </label>
                                                <input name="vice_principal_name" value="{{ old('vice_principal_name') }}" id="vice_principal_name" type="text" class="form-control">
                                                @error('vice_principal_name')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="vice_principal_birthday">Ngày sinh hiệu phó </label>
                                                <input name="vice_principal_birthday" value="{{ old('vice_principal_birthday') }}" id="vice_principal_birthday" type="text" class="datepicker form-control" placeholder="dd/mm/yyyy">
                                                @error('vice_principal_birthday')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="vice_principal_phone">Số điện thoại hiệu phó </label>
                                                <input name="vice_principal_phone" value="{{ old('vice_principal_phone') }}" id="vice_principal_phone" type="text" class="form-control">
                                                @error('vice_principal_phone')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="vice_principal_email">Email hiệu phó </label>
                                                <input name="vice_principal_email" value="{{ old('vice_principal_email') }}" id="vice_principal_email" type="text" class="form-control">
                                                @error('vice_principal_email')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.school.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
