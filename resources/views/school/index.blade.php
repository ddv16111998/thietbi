@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Trường học @endsection
@section('css')
@endsection
@section('js')
    <script>
    $(function() {
        $('#listSchool').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.school.index') !!}',

            columns: [
                { data: 'id', name: 'id'},
                { data: 'name', name: 'name'},
                { data: 'principal_name', name: 'principal_name'},
                { data: 'address', name: 'address'},
                { data: 'email', name: 'email'},
                { data: 'hotline', name: 'hotline'},
                { data: 'level', name: 'level'},
                { data: 'year_link', name: 'year_link'},
                { data: 'action', name: 'action'}
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    });
    </script>

    <script>
        function getDetailSchool(id) {
            var csrf = '@csrf';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': csrf.value
                }
            });
            $.ajax({
                'url' : '{{ route('admin.school.index') }}',
                'data': {
                    'id' : id,
                },
                success: function (data) {
                    console.log(data);
                    $('#image_maps').attr('src', data['school_detail']['image_maps']);
                    $('#image_schedule').attr('src', data['school_detail']['image_schedule']);
                    $('#name').html(data['school']['name']);
                    $('#principal_name').html(data['school']['principal_name']);
                    $('#address').html(data['school']['address']);
                    $('#email').html(data['school']['email']);
                    $('#hotline').html(data['school']['hotline']);
                    $('#level').html(data['school_detail']['level']);
                    $('#year_link').html(data['school_detail']['year_link']);
                    $('#total_class').html(data['school_detail']['total_class']);
                    $('#principal_birthday').html(data['school_detail']['principal_birthday']);
                    $('#principal_phone').html(data['school_detail']['principal_phone']);
                    $('#principal_email').html(data['school_detail']['principal_email']);
                    $('#vice_principal_name').html(data['school_detail']['vice_principal_name']);
                    $('#vice_principal_birthday').html(data['school_detail']['vice_principal_birthday']);
                    $('#vice_principal_phone').html(data['school_detail']['vice_principal_phone']);
                    $('#vice_principal_email').html(data['school_detail']['vice_principal_email']);
                    // $('.modal-body').html(data['school_detail']['vice_principal_name']);
                }
            });
        };
    </script>
@endsection
@section('content')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">
                    Quản lý trường học
                </h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.school.index') }}">QL trường học</a></li>
                </ol>
                <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.school.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listSchool" class="table table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <th>Tên trường học</th>
                                        <th>Hiệu trưởng</th>
                                        <th>Địa chỉ</th>
                                        <th>Email</th>
                                        <th>Hotline</th>
                                        <th>Cấp</th>
                                        <th>Năm học</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                </table>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="detailSchool" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left"><span id="name"></span></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
<div class="card-body">
    <div class="justify-content-md-center">
        <div class="col-md-12 row form-group">
            <div class="col-sm-6">
                <div class="form-group">
                    <b><label for="">Bản đồ</label></b><br>
                    <img src="" alt="" id="image_maps" style="width:200px;cursor:pointer;">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <b><label>Thời khóa biểu</label></b><br>
                    <img src="" alt="" id="image_schedule" style="width:200px;cursor:pointer;">
                </div>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Hiệu trưởng:</b>
            </div>
            <div class="col-6">
                <span id="principal_name">principal_name</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Địa chỉ trường:</b>
            </div>
            <div class="col-6">
                <span id="address">address</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Email:</b>
            </div>
            <div class="col-6">
                <span id="email">email</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Hotline:</b>
            </div>
            <div class="col-6">
                <span id="hotline">hotline</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Cấp:</b>
            </div>
            <div class="col-6">
                <span id="level">level</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Năm học:</b>
            </div>
            <div class="col-6">
                <span id="year_link">year_link</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Tổng số lớp học:</b>
            </div>
            <div class="col-6">
                <span id="total_class">total_class</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Ngày sinh hiệu trưởng:</b>
            </div>
            <div class="col-6">
                <span id="principal_birthday">principal_birthday</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Số điện thoại hiệu trưởng:</b>
            </div>
            <div class="col-6">
                <span id="principal_phone">principal_phone</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Email hiệu trưởng:</b>
            </div>
            <div class="col-6">
                <span id="principal_email">principal_email</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Hiệu phó:</b>
            </div>
            <div class="col-6">
                <span id="vice_principal_name">vice_principal_name</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Ngày sinh hiệu phó:</b>
            </div>
            <div class="col-6">
                <span id="vice_principal_birthday">vice_principal_birthday</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Số điện thoại hiệu phó:</b>
            </div>
            <div class="col-6">
                <span id="vice_principal_phone">vice_principal_phone</span>
            </div>
        </div><hr>
        <div class="row form-group">
            <div class="col-3">
                <b>Email hiệu phó:</b>
            </div>
            <div class="col-6">
                <span id="vice_principal_email">vice_principal_email</span>
            </div>
        </div>
    </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>
@endsection
