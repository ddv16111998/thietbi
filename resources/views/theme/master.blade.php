<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/favicon.ico" type="image/x-icon" rel="icon">
    <link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <title>@yield('title')</title>
    @yield('meta')
    <!-- Plugins css -->
    <link href="/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    {{--    --}}

    <link href="/assets/css/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/morris.css" rel="stylesheet">
    <link href="/assets/css/jquery.toast.css" rel="stylesheet">
    <link href="/assets/css/style.min.css" rel="stylesheet">
    <link href="/assets/css/dashboard1.css" rel="stylesheet">
    <link href="/assets/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/fontawesome/css/fontawesome.min.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
    <link href="/assets/css/crud.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=vietnamese" rel="stylesheet">
    <style>
        {!! config('theme.'.Auth::user()->theme->theme) !!}
    </style>
</head>
<body class="skin-default-dark fixed-layout">
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Loading...</p>
    </div>
</div>
<div id="main-wrapper">
    @include('layouts.top_bar')
    @include('layouts.left_sidebar')
    <div class="page-wrapper">
        <div class="container-fluid" id="main__content">
            @yield('content')
        </div>
    </div>
    @include('layouts.footer')
</div>
{{--<script src="/assets/js/jquery-3.2.1.min.js"></script>--}}
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/sidebarmenu.js"></script>
<script src="/assets/js/custom.min.js"></script>
<script src="/assets/js/raphael-min.js"></script>
<script src="/assets/js/jquery.sparkline.min.js"></script>
<script src="/assets/js/jquery.toast.js"></script>
<script src="/assets/js/jquery.peity.min.js"></script>
<script src="/assets/js/jquery.peity.init.js"></script>
<script src="/assets/js/select2.min.js"></script>
<!-- plugin js -->
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
<script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
{{-- datatables --}}
<script src="/assets/js/pdfmake.min.js"></script>
<script src="/assets/js/vfs_fonts.js"></script>
<script src="/assets/js/datatables.min.js"></script>


<script src="/assets/js/crud.js"></script>
<script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
@yield('js')
<script>
    @if(Session::has('message'))
        var type = "{{ Session::get('type', 'success') }}";
        var message = "{{ Session::get('message') }}";
        notification(type, message);
    @endif
</script>
</body>
</html>
