@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Thêm mới nhân viên')
@section('css')
    <style>
        #main__content {
            max-width: 1000px;
            margin-top: 50px;
        }
    </style>
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Thêm mới</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.user.index') }}">Quản lý nhân viên</a></li>
                        <li class="breadcrumb-item active"><a href="">Thêm mới</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.user.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    @if ($errors->any())
                                      <div class="alert alert-danger">
                                          <ul>
                                              @foreach ($errors->all() as $error)
                                                  <li>{{ $error }}</li>
                                              @endforeach
                                          </ul>
                                      </div>
                                    @endif
                                </div>
                                <div class="col-md-12 row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Avatar</label>
                                            <div>
                                                <img src="/images/no-image-icon.png" class="img_change" onclick="$('.inp__img').click()" style="width:200px;cursor:pointer;">
                                                <input type="file" name="avatar" class="inp__img hide">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tên nhân viên<span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control form-control-line" value="{{ old('name') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Tài khoản<span class="text-danger">*</span></label>
                                            <input type="text" name="acc" class="form-control form-control-line" value="{{ old('acc') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email">Email<span class="text-danger">*</span></label>
                                            <input type="email" name="email" id="example-email2" class="form-control"  value="{{ old('email') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Password<span class="text-danger">*</span></label>
                                            <input type="password" name="password" class="form-control" placeholder="Enter Password" value="{{ old('password') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nhập lại password<span class="text-danger">*</span></label>
                                            <input type="password" name="password_verified" class="form-control" placeholder="Enter Password" value="{{ old('password_verified') }}">
                                        </div>
                                    </div>
                                </div>
                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.user.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
