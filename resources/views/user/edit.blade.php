@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Sửa thông tin nhân viên')
@section('css')
    <style>
        #main__content {
            max-width: 1000px;
            margin-top: 50px;
        }
        .btn__chage span {
            cursor: pointer;
            color: #03a9f3;
            font-size: 14px;
        }
    </style>
@endsection
@section('js')
    <script>
        var html = '<div class="col-md-12 row">\
                        <div class="col-sm-6">\
                            <div class="form-group">\
                                <label>Password<span class="text-danger">*</span></label>\
                                <input type="password" name="password" class="form-control" autocomplete="off"> \
                            </div>\
                        </div>\
                        <div class="col-sm-6">\
                            <div class="form-group">\
                                <label>Nhập lại password<span class="text-danger">*</span></label>\
                                <input type="password" name="password_verified" class="form-control" autocomplete="off"> \
                            </div>\
                        </div>\
                    </div>';
        $(document).on('click', '.btn__change_password', function(){
            $('#main-content .left-form .form__info').hide();
            $('#main-content .left-form .change__password').html(html);
            $(this).text('Hủy').attr('class', 'btn__cancel_change_password');
        });
        $(document).on('click', '.btn__cancel_change_password' , function(){
            $('#main-content .left-form .change__password .row').remove();
            $('#main-content .left-form .form__info').show();
            $(this).text('Đổi mật khẩu').attr('class', 'btn__change_password');
        });
    </script>
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Sửa thông tin</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.user.index') }}">Quản lý nhân viên</a></li>
                        <li class="breadcrumb-item active"><a href="">Sửa thông tin</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.user.update', $user['id']) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    @if ($errors->any())
                                      <div class="alert alert-danger">
                                          <ul>
                                              @foreach ($errors->all() as $error)
                                                  <li>{{ $error }}</li>
                                              @endforeach
                                          </ul>
                                      </div>
                                    @endif
                                </div>
                                <div class="col-md-12 row form__info">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Avatar</label>
                                            <div>
                                                <img src="@if($user['avatar'] AND file_exists(public_path($user['avatar']))) {{$user['avatar']}} @else {{'/images/no-image-icon.png'}} @endif" class="img_change" onclick="$('.inp__img').click()" style="width:200px;cursor:pointer;">
                                                <input type="file" name="avatar" class="inp__img hide">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tên nhân viên<span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control form-control-line" value="{{ $user['name'] }}"> 
                                        </div>
                                        <div class="form-group">
                                            <label>Tài khoản<span class="text-danger">*</span></label>
                                            <input type="text" name="acc" class="form-control form-control-line" value="{{ $user['acc'] }}"> 
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email">Email<span class="text-danger">*</span></label>
                                            <input type="email" name="email" id="example-email2" autocomplete="off" class="form-control"  value="{{ $user['email'] }}"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 row change__password"></div>
                                <div class="row btn__chage">
                                    <span class="btn__change_password">Đổi mật khẩu</span>
                                </div>
                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.user.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
