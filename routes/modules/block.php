<?php 
Route::group(['prefix'=>'block', 'middleware' => 'permission'], function () {
	Route::get('', 'BlockController@index')->name('admin.block.index');
	Route::get('/create', 'BlockController@create')->name('admin.block.create');
	Route::post('/store', 'BlockController@store')->name('admin.block.store');
	Route::get('/edit/{id?}', 'BlockController@edit')->name('admin.block.edit');
	Route::post('/update/{id?}', 'BlockController@update')->name('admin.block.update');
	Route::get('/destroy/{id?}', 'BlockController@destroy')->name('admin.block.destroy');
});