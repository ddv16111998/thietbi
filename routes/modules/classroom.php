<?php
Route::group(['prefix' => 'classroom','middleware' => 'permission'] ,function(){
    Route::get('/','ClassroomController@index')->name('admin.classroom.index');
    Route::get('create', 'ClassroomController@create')->name('admin.classroom.create');
    Route::post('store', 'ClassroomController@store')->name('admin.classroom.store');
    Route::get('edit/{id}', 'ClassroomController@edit')->name('admin.classroom.edit');
    Route::post('update/{id}', 'ClassroomController@update')->name('admin.classroom.update');
    Route::get('destroy/{id}', 'ClassroomController@destroy')->name('admin.classroom.destroy');
})
?>
