<?php

Route::get('/dashboard', 'HomeController@index')->name('admin.dashboard');
Route::get('/get_location', 'HomeController@get_location')->name('get_location');
Route::get('/get_info', 'HomeController@get_info')->name('get_info');
Route::get('/change-theme/{change}', 'HomeController@changeTheme')->name('admin.change-theme');
