<?php 
Route::group(['prefix'=>'devicedown', 'middleware' => 'permission'], function () {
	Route::get('', 'DevicedownController@index')->name('admin.devicedown.index');
	Route::get('/create', 'DevicedownController@create')->name('admin.devicedown.create');
	Route::post('/store', 'DevicedownController@store')->name('admin.devicedown.store');
	Route::get('/edit/{id?}', 'DevicedownController@edit')->name('admin.devicedown.edit');
	Route::post('/update/{id?}', 'DevicedownController@update')->name('admin.devicedown.update');
	Route::get('/destroy/{id?}', 'DevicedownController@destroy')->name('admin.devicedown.destroy');

	Route::get('/exportExcel', 'DevicedownController@exportExcel')->name('admin.devicedown.excel');
});