<?php
	Route::group(['prefix' => 'devices','middleware' => 'permission'] ,function(){
		// Quản lý thiết bị
		Route::get('/','DevicesController@index')->name('admin.devices.index');
		Route::get('create', 'DevicesController@create')->name('admin.devices.create');
		Route::post('store', 'DevicesController@store')->name('admin.devices.store');
		Route::get('edit/{id}', 'DevicesController@edit')->name('admin.devices.edit');
		Route::post('update/{id}', 'DevicesController@update')->name('admin.devices.update');
		Route::get('destroy/{id}', 'DevicesController@destroy')->name('admin.devices.destroy');


		// Quản lý mượn trả thiết bị
		Route::group(['prefix' => 'borrow'], function(){
			route::get('/','DevicesController@borrowIndex')->name('admin.borrow-and-return.index');
			route::get('registration','DevicesController@registrationBorrow')->name('admin.borrow-and-return.create');
			route::post('registration','DevicesController@handleBorrow')->name('admin.borrow-and-return.store');
			route::get('edit/{id}','DevicesController@editBorrow')->name('admin.borrow-and-return.edit');
			route::post('update/{id}','DevicesController@updateBorrow')->name('admin.borrow-and-return.update');
			route::get('destroy/{id}','DevicesController@destroyBorrow')->name('admin.borrow-and-return.destroy');
			route::get('create-fast/{id}','DevicesController@createFast')->name('admin.borrow-and-return.create_fast');
			route::get('create-broken-lost/{id}','DevicesController@createBrokenLost')->name('admin.borrow-and-return.create_broken_lost');
		});


		//Quản lý sửa chữa thiết bị
		Route::group(['prefix'=>'repair'],function(){
			route::get('/','DevicesController@repairIndex')->name('admin.repair.index');
			route::get('create','DevicesController@repairCreate')->name('admin.repair.create');
			route::post('store','DevicesController@repairStore')->name('admin.repair.store');
			route::get('edit/{id}','DevicesController@repairEdit')->name('admin.repair.edit');
			route::post('update/{id}','DevicesController@repairUpdate')->name('admin.repair.update');
            route::get('destroy/{id}','DevicesController@repairDestroy')->name('admin.repair.destroy');
            route::get('follow/{id}','DevicesController@repairFollow')->name('admin.repair.follow');
            route::get('update-status/{id?}/{status?}','DevicesController@changeStatus')->name('admin.repair.update-status');
            route::get('track-book','DevicesController@trackBook')->name('admin.repair.track_book');
            route::get('export-excel','DevicesController@exportExcel')->name('admin.repair.excel');
		});
	});


	// Route em phải luôn để ở dạng admin.module.index admin.module.create ...
	// index create store destroy update edit là bắt buộc nhé
	// module thì tương ứng với chức năng em làm
 ?>
