<?php 
Route::group(['prefix'=>'permisson','middleware' => 'permission'], function () {
	Route::get('', 'permissonController@index')->name('admin.permisson.index');
	Route::get('/create', 'permissonController@create')->name('admin.permisson.create');
	Route::post('/store', 'permissonController@store')->name('admin.permisson.store');
	Route::get('/edit/{id?}', 'permissonController@edit')->name('admin.permisson.edit');
	Route::post('/update/{id?}', 'permissonController@update')->name('admin.permisson.update');
	Route::get('/destroy/{id?}', 'permissonController@destroy')->name('admin.permisson.destroy');
});