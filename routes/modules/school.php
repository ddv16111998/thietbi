<?php 
Route::group(['prefix'=>'school','middleware' => 'permission'], function () {
	Route::get('', 'SchoolController@index')->name('admin.school.index');
	Route::get('/getDetail', 'SchoolController@getDetail')->name('admin.school.getDetail');
	Route::get('create', 'SchoolController@create')->name('admin.school.create');
	Route::post('store', 'SchoolController@store')->name('admin.school.store');
	Route::get('edit/{id?}', 'SchoolController@edit')->name('admin.school.edit');
	Route::post('update/{id?}', 'SchoolController@update')->name('admin.school.update');
	Route::get('destroy/{id?}', 'SchoolController@destroy')->name('admin.school.destroy');
});