<?php
Route::group(['prefix' => 'subject','middleware' => 'permission'] ,function(){
    Route::get('/','SubjectController@index')->name('admin.subject.index');
    Route::get('create', 'SubjectController@create')->name('admin.subject.create');
    Route::post('store', 'SubjectController@store')->name('admin.subject.store');
    Route::get('edit/{id}', 'SubjectController@edit')->name('admin.subject.edit');
    Route::post('update/{id}', 'SubjectController@update')->name('admin.subject.update');
    Route::get('destroy/{id}', 'SubjectController@destroy')->name('admin.subject.destroy');
})
?>
