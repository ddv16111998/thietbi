<?php
Route::group(['prefix' => 'user','middleware' => 'permission'], function() {
    Route::get('/', 'UserController@index')->name('admin.user.index');
    Route::get('create', 'UserController@create')->name('admin.user.create');
    Route::post('store', 'UserController@store')->name('admin.user.store');
    Route::get('edit/{id}', 'UserController@edit')->name('admin.user.edit')->where('id','[0-9]+');
    Route::post('update/{id}', 'UserController@update')->name('admin.user.update')->where('id','[0-9]+');
    Route::get('destroy/{id}','UserController@destroy')->name('admin.user.destroy')->where('id','[0-9]+');
    Route::post('role','UserController@selectRole')->name('admin.user.role');
});
