<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('',function(){
	return view('home');
});

Route::get('cap-truong', function() {
  return view('captruong');
})->name('cap-truong');

Route::get('cap-bo', function() {
  return view('capbo');
})->name('cap-bo');

Route::get('cap-so', function() {
  return view('capso');
})->name('cap-so');

Route::get('cap-phong', function() {
  return view('capphong');
})->name('cap-phong');


Route::group(['prefix' => 'admin'], function(){
	Auth::routes();
	Route::middleware(['logincheck'])->group(function () {
		Route::get('', function () {
			return redirect('admin/login');
		});
		foreach(glob(base_path('routes')."/modules/*.php") as $file){
		    require $file;
		}
	});
});

